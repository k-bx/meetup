ifndef STACK_ROOT
    export STACK_ROOT:=$(shell echo "$$(echo ~)/.stack")
endif

ifndef MEETUP_HOST
    export MEETUP_HOST:=meetup
endif

# STACK_ROOT := $(if $$STACK_ROOT,$$STACK_ROOT,$$HOME/.stack)

fast:
	make fast-be
	make fast-fe
	echo "Start web server with: (cd dist && ./meetup > $pwd/logs/meetup.txt)"
.PHONY: fast

prod:
	make pre
	cd backend && make prod
	make generate-elm
	cd frontend && make prod
	make post

pre:
	rm -rf ./dist
	mkdir -p ./dist/frontend
	mkdir -p ./dist/sysadmin
.PHONY: pre

post:
	cd frontend && cp -r ./dist ../
	cd backend/meetup && cp $$(stack exec --allow-different-user -- which meetup) ../../dist
	cp -r ./sysadmin/keys ./dist/sysadmin/
.PHONY: post

fast-fe:
	make pre
	make generate-elm
	cd frontend && make build
	make post
.PHONY: fast

fast-be:
	make pre
	cd backend && make fast
	make post
.PHONY: fast-be

run-be:
	cd backend/meetup && $$(stack exec --allow-different-user -- which meetup)
.PHONY: run-be

run-dev:
	# sudo apt install inotify-tools
	./run-dev.sh
.PHONY: run-dev

generate-elm:
	cd backend/meetup && $$(stack exec --allow-different-user -- which meetup) generate-elm > ../../frontend/src/Le/.Api.elm
	mv frontend/src/Le/.Api.elm frontend/src/Le/Api.elm
.PHONY: generate-elm

deploy:
	# ssh ubuntu@${MEETUP_HOST} rm -rf /home/ubuntu/meetup
	ssh ubuntu@${MEETUP_HOST} mkdir -p /home/ubuntu/meetup
	rsync -vrzah dist/ ubuntu@${MEETUP_HOST}:/home/ubuntu/meetup
	ssh ubuntu@${MEETUP_HOST} sudo systemctl restart meetup
.PHONY: deploy

sysadmin-setup:
	ssh ubuntu@meetup sudo apt-get install -y libpq-dev
	scp sysadmin/meetup.service root@meetup:/etc/systemd/system/meetup.service
	ssh ubuntu@meetup sudo systemctl daemon-reload
	ssh ubuntu@meetup sudo systemctl enable meetup.service
	# decided to not include the postgresql setup, because it should
	# be moved out eventually anyways

	cat ~/Dropbox/gfhjkb/meetup/envvars_prod | cut -d' ' -f2- > envvars_tmp
	scp envvars_tmp ubuntu@meetup:
	rm envvars_tmp
	ssh ubuntu@meetup sudo mv /home/ubuntu/envvars_tmp /etc/meetup.env
	ssh ubuntu@meetup sudo systemctl restart meetup
.PHONY: sysadmin-setup

migrate:
	cd dist && ./meetup migrate

migrate-prod:
	ssh -t ubuntu@meetup "cd /home/ubuntu/meetup && ./meetup migrate"

psql-prod:
	ssh -t ubuntu@meetup "psql -h localhost -U postgres -W meetup"

copy-prod-db:
	ssh -t ubuntu@meetup "PGPASSWORD=password pg_dump -h localhost -U postgres meetup | gzip > meetup.pgdump.gz"
	scp ubuntu@meetup:meetup.pgdump.gz ~/tmp/meetup.pgdump.gz
	PGPASSWORD=password dropdb -h localhost -U postgres meetup
	PGPASSWORD=password psql -h localhost -U postgres -c 'CREATE DATABASE meetup'
	gunzip -c ~/tmp/meetup.pgdump.gz | PGPASSWORD=password psql -h localhost -U postgres meetup

tags:
	hasktags -e .
	mv TAGS TAGS01
	find . -type f -name "*.elm" -print | etags --language=none --regex=@elm.tags -
	mv TAGS TAGS02
	cat TAGS02 >> TAGS
	rm TAGS02
	cat TAGS01 >> TAGS
	rm TAGS01
.PHONY: tags

.PHONY: docker
docker:
	@echo "> STACK_ROOT IS: $$STACK_ROOT"
	docker build -t meetup/meetup-builder:latest -f Dockerfile.builder .
	docker run -t --rm \
		-v "$$(pwd)":/src \
		-v "$$STACK_ROOT:$$STACK_ROOT" \
		-e "STACK_ROOT=$$STACK_ROOT" \
		--ulimit nofile=10000 \
		meetup/meetup-builder build
	@$(MAKE) --no-print-directory clean_docker_images

.PHONY: clean_docker_images
clean_docker_images:
	@docker rmi $$(docker images -f "dangling=true" -q) >/dev/null 2>/dev/null || true
