{-# OPTIONS_GHC -fno-warn-orphans #-}

{- Note [TimeZone Conversion]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It happens so that Haskell has few different types for handling time
zones. One is 'TimeZone' in 'time' package, another is 'TZ' from 'tz'
package. You can get former from latter by calling
'Data.Time.Zones.timeZoneForUTCTime :: TZ -> UTCTime -> TimeZone'. As
one can see, it requires you to provide a timestamp **when** you need
the timezone, since it might differ at a different point in time.

We make a shortcut by using the UTC version of time to get that zone,
even though in theory it might differ, it seems to be good enough to
work in all cases for us.
-}

{- Note [TimeZone and Wire]

We pass time in number of milliseconds over the wire that's already
zoned, so we parse it as UTCTime and then convert into ZonedTime
without the usual zone arithmetics. Use 'localTimeToUTCTZ' to further
get the 'TimeZone' object.
-}

module Data.Time.Additional where

import Control.Concurrent (threadDelay)
import qualified Data.Fixed
import Data.Hashable
import Data.Time
import Data.Time.Calendar.WeekDate
import qualified Data.Time.Clock as Time
import Data.Time.Clock.POSIX
import qualified Data.Time.Clock.POSIX as Time
import qualified Data.Time.Zones
import Prelude

nominalDiffTimeToMilliseconds :: Time.NominalDiffTime -> Int
nominalDiffTimeToMilliseconds t =
  let Data.Fixed.MkFixed s = nominalDiffTimeToSeconds t
   in fromIntegral (s `div` ((10 :: Integer) ^ (9 :: Int)))

millisecondsToNominalDiffTime :: Int -> Time.NominalDiffTime
millisecondsToNominalDiffTime t = fromIntegral (t `div` 1000)

utcTimeToMilliseconds :: Time.UTCTime -> Int
utcTimeToMilliseconds =
  nominalDiffTimeToMilliseconds . Time.utcTimeToPOSIXSeconds

millisecondsToUtcTime :: Int -> Time.UTCTime
millisecondsToUtcTime =
  Time.posixSecondsToUTCTime . millisecondsToNominalDiffTime

-- | See Note [TimeZone and Wire]
zonedTimeToMilliseconds :: ZonedTime -> Int
zonedTimeToMilliseconds (ZonedTime localTime _tz) =
  utcTimeToMilliseconds (localTimeToUTC utc localTime)

-- | If you got milliseconds that you know are zoned over the wire,
-- construct a zoned time
--
-- See Note [TimeZone Conversion]
millisecondsToZonedTime :: Data.Time.Zones.TZ -> Int -> ZonedTime
millisecondsToZonedTime tz' ms =
  let utctime = millisecondsToUtcTime ms
      tz'' = Data.Time.Zones.timeZoneForUTCTime tz' utctime
   in ZonedTime (utcToLocalTimeNoConv utctime) tz''

addZonedTime :: NominalDiffTime -> ZonedTime -> ZonedTime
addZonedTime d (ZonedTime localTime tz) =
  ZonedTime (addLocalTime d localTime) tz

-- | See Note [TimeZone and Wire]
utcToLocalTimeNoConv :: UTCTime -> LocalTime
utcToLocalTimeNoConv (UTCTime day dt) = LocalTime day (timeToTimeOfDay dt)

-- | See Note [TimeZone Conversion]
utcToZonedTime' :: Data.Time.Zones.TZ -> UTCTime -> ZonedTime
utcToZonedTime' tz t =
  utcToZonedTime (Data.Time.Zones.timeZoneForUTCTime tz t) t

-- | Format taken by threadDelay and friends
nominalDiffToMicroSeconds :: Integral a => NominalDiffTime -> a
nominalDiffToMicroSeconds x = truncate ((realToFrac x :: Double) * 1000000)

threadDelaySecs :: NominalDiffTime -> IO ()
threadDelaySecs n = threadDelay (nominalDiffToMicroSeconds n)

-- | Finds the immediately preceding 'UTCTime' value which represents the
-- beginning of a ISO 8601 Week (i.e., is a Monday) and has no fractional
-- daypart.
--
-- >>> rewindToWeek $ read "2015-06-30 23:59:60.123 UTC"
-- 2015-06-29 00:00:00 UTC
rewindToWeek :: UTCTime -> UTCTime
rewindToWeek (UTCTime day _) =
  let (year, week, _) = toWeekDate day
   in UTCTime (fromWeekDate year week 1) 0

-- | Assuming Monday here, might change in future
rewindDayToWeek :: Day -> Day
rewindDayToWeek day =
  let subtr =
        case dayOfWeek day of
          Monday -> 0
          Tuesday -> 1
          Wednesday -> 2
          Thursday -> 3
          Friday -> 4
          Saturday -> 6
          Sunday -> 7
   in addDays ((-1) * subtr) day

rewindToWeekLocal :: LocalTime -> LocalTime
rewindToWeekLocal (LocalTime day _timeOfDay) =
  LocalTime (rewindDayToWeek day) (TimeOfDay 0 0 0)

rewindToDayLocal :: LocalTime -> LocalTime
rewindToDayLocal (LocalTime day _timeOfDay) =
  LocalTime day (TimeOfDay 0 0 0)

rewindToDayZoned :: ZonedTime -> ZonedTime
rewindToDayZoned (ZonedTime localTime tz) =
  ZonedTime (rewindToDayLocal localTime) tz

rewindToWeekZoned :: ZonedTime -> ZonedTime
rewindToWeekZoned (ZonedTime localTime tz) =
  ZonedTime (rewindToWeekLocal localTime) tz

-- | Collisions occur between all fractional seconds and between leap seconds
-- and their succeeding seconds. Contrast with the performance of a 'show'
-- based hash.
instance Hashable UTCTime where
  hashWithSalt salt = hashWithSalt salt . toRational . utcTimeToPOSIXSeconds

instance Hashable ZonedTime where
  hashWithSalt salt = hashWithSalt salt . zonedTimeToUTC

instance Eq ZonedTime where
  z1 == z2 = zonedTimeToUTC z1 == zonedTimeToUTC z2

instance Hashable Day where
  hashWithSalt salt = hashWithSalt salt . toModifiedJulianDay

deriving instance Ord ZonedTime
