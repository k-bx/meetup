{-# OPTIONS_GHC -fno-warn-orphans #-}

module Le.Types.TimeZones where

import qualified Data.Aeson as J
import qualified Data.String.Class as S
import Data.Time.Zones.DB (TZLabel)
import Database.Persist
import Database.Persist.Sql
import Le.Utils
import RIO

instance J.ToJSON TZLabel where
  toEncoding = J.toEncoding . J.String . tshow

instance J.FromJSON TZLabel where
  parseJSON v = do
    t <- J.parseJSON v
    either (fail . S.toString) pure (readEitherSafeText t)

instance PersistField TZLabel where
  toPersistValue x = toPersistValue (tshow x)
  fromPersistValue v = do
    t <- fromPersistValue v
    readEitherSafeText t

instance PersistFieldSql TZLabel where
  sqlType _ = SqlString
