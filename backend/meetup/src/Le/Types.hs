{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE TemplateHaskell #-}

module Le.Types where

import Control.Newtype
import qualified Data.Aeson as J
import Data.Pool (Pool)
import qualified Data.String.Class as S
import Database.Persist
import qualified Database.Persist.Postgresql as P
import Database.Persist.Sql
import Elm.Derive
import Le.Aeson
import Le.Utils
import qualified Network.HTTP.Client as HTTPClient
import Network.URI (URI, parseAbsoluteURI)
import RIO
import Servant.API (FromHttpApiData, ToHttpApiData)
import Web.PathPieces (PathPiece)

type AppM = ReaderT Env IO

data EnvConfig =
  EnvConfig
    { cfgMailgunDomain :: Text
    , cfgMailgunApiKey :: Text
    , cfgFacebookAppId :: Text
    , cfgFacebookAppSecret :: Text
    , cfgFacebookAppToken :: Text
    , cfgGoogleOauthClientId :: Text
    , cfgGoogleOauthClientSecret :: Text
    , cfgCertPath :: Text
    , cfgKeyPath :: Text
    , cfgHttpsPort :: Int
    , cfgHttpPort :: Int
    }

data Env =
  Env
    { envDb :: Pool P.SqlBackend
    , envLogFunc :: RIO.LogFunc
    , envConfig :: EnvConfig
    , envHttpManager :: HTTPClient.Manager
    }

instance HasLogFunc Env where
  logFuncL = lens envLogFunc (\v a -> v {envLogFunc = a})

data Condition
  = New
  | LikeNew
  | VeryGood
  | Good
  | Acceptable
  | BetterThanNothing
  deriving (Eq, Show, Read, Generic)

deriveBoth (jsonOpts 0) ''Condition

instance PersistField Condition where
  toPersistValue x = toPersistValue (tshow x)
  fromPersistValue v = do
    t <- fromPersistValue v
    readEitherSafeText t

instance PersistFieldSql Condition where
  sqlType _ = SqlString

newtype LoginTokenVal =
  LoginTokenVal Text
  deriving ( Show
           , Eq
           , Read
           , PersistField
           , PersistFieldSql
           , Ord
           , PathPiece
           , ToHttpApiData
           , FromHttpApiData
           , J.ToJSON
           , J.FromJSON
           )

instance Newtype LoginTokenVal Text

newtype PersistentAbsoluteURI =
  PersistentAbsoluteURI URI
  deriving (Show, Eq)

newtype PhoneNumber =
  PhoneNumber Text
  deriving (Show, Eq, PersistFieldSql, PersistField)

instance Newtype PhoneNumber Text

newtype FullName =
  FullName Text
  deriving (Show, Eq, PersistFieldSql, PersistField)

instance Newtype FullName Text

instance Newtype PersistentAbsoluteURI URI

instance PersistField PersistentAbsoluteURI where
  toPersistValue x = toPersistValue (tshow (unpack x))
  fromPersistValue v = do
    t <- fromPersistValue v
    let mUri = parseAbsoluteURI (S.fromText t)
    maybe
      (Left (S.toText ("Failed to parse an absolute URI: " <> (S.toString t))))
      (return . pack)
      mUri

instance PersistFieldSql PersistentAbsoluteURI where
  sqlType _ = SqlString

data ApprovalStatus
  = ApprovalStatusApproved
  | ApprovalStatusDeclined
  deriving (Show, Eq, Read)

instance PersistFieldSql ApprovalStatus where
  sqlType _ = SqlString

instance PersistField ApprovalStatus where
  toPersistValue x = toPersistValue (tshow x)
  fromPersistValue v = do
    t <- fromPersistValue v
    readEitherSafeText t
