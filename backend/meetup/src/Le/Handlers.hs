{-# LANGUAGE TypeApplications #-}

module Le.Handlers where

import Control.Monad.Logger (MonadLogger (..), toLogStr)
import Control.Newtype
import qualified Data.Binary.Builder
import qualified Data.Char as Char
import qualified Data.Map.Strict as Map
import qualified Data.String.Class as S
import qualified Data.Text.IO as T
import qualified Data.Time.Zones.All
import qualified Data.UUID as UUID
import qualified Data.UUID.V4 as UUID4
import qualified Database.Persist.Postgresql as P
import qualified Le.ApiTypes as AT
import Le.AppUtils
import Le.BusinessLogic
import qualified Le.Emails
import Le.Handlers.Common
import Le.Import
import qualified Le.Links
import Le.Model
import qualified Le.Queries as Q
import Le.Renderers
import Le.Types
import Le.Validators
import Network.Wai (Request, requestHeaders)
import Servant
import Servant.Server.Experimental.Auth
  ( AuthHandler,
    AuthServerData,
    mkAuthHandler,
  )
import System.Log.FastLogger (fromLogStr)
import qualified Test.RandomStrings as RandomStrings
import qualified Text.Blaze as Blaze
import qualified Text.Blaze.Html5 as Blaze
import qualified Text.Blaze.Html5.Attributes as Blaze
import qualified Web.Cookie as Cookie

data RioApp
  = RioApp
      { appLogFunc :: LogFunc
      }

instance MonadLogger (RIO RioApp) where
  monadLoggerLog _loc _logSource _logLevel msg =
    liftIO (S.putStrLn (fromLogStr (toLogStr msg)))

instance HasLogFunc RioApp where
  logFuncL = lens appLogFunc (\x y -> x {appLogFunc = y})

-- | We need to specify the data returned after authentication
type instance
  AuthServerData (AuthProtect "cookie-auth") =
    P.Entity User

indexEndpoint :: AppM Text
indexEndpoint = liftIO $ T.readFile "index.html"

pongEndpoint :: AppM Text
pongEndpoint = return "pong"

blazeBootstrapWrap :: Blaze.Html -> Blaze.Html
blazeBootstrapWrap x =
  Blaze.html $ do
    Blaze.head $ do
      Blaze.link Blaze.! Blaze.rel "stylesheet" Blaze.! Blaze.type_ "text/css"
        Blaze.! Blaze.href "/vendor/bootstrap.min.css"
    Blaze.body $ do Blaze.div Blaze.! Blaze.class_ "container" $ do { x }

meetupsList :: AppM [AT.Meetup]
meetupsList = do
  meetups <- runDb $ Q.latestMeetups
  return (map renderMeetup meetups)

meetupInfo :: MeetupId -> AppM AT.Meetup
meetupInfo meetupId = do
  meetup <- mustFindM $ runDb $ P.getEntity meetupId
  return $ renderMeetup meetup

meetupAuthInfo :: Entity User -> MeetupId -> AppM AT.MeetupAuthInfo
meetupAuthInfo user meetupId =
  sg $ do
    membershipId <-
      fmap (fmap entityKey)
        $ runDb
        $ P.selectFirst
          [ ApprovedMemberUserId P.==. entityKey user,
            ApprovedMemberMeetupId P.==. meetupId
          ]
          []
    mform <- runDb $ blGetMeetupApplicationForm meetupId
    userApplicationId <-
      case mform of
        Nothing -> pure Nothing
        Just form ->
          fmap (fmap entityKey)
            $ runDb
            $ P.selectFirst
              [ UserApplicationFormId P.==. entityKey form,
                UserApplicationUserId P.==. entityKey user
              ]
              []
    mApprovedMember <-
      runDb $
        P.selectFirst
          [ ApprovedMemberMeetupId P.==. meetupId,
            ApprovedMemberUserId P.==. entityKey user
          ]
          []
    let membershipApproved =
          case mApprovedMember of
            Nothing -> Nothing
            Just x ->
              case approvedMemberStatus (ev x) of
                ApprovalStatusApproved -> Just True
                ApprovalStatusDeclined -> Just False
    pure $
      AT.MeetupAuthInfo
        { maiId = meetupId,
          maiApprovedMemberId = membershipId,
          maiUserApplicationId = userApplicationId,
          maiMembershipApproved = membershipApproved
        }

eventsList :: MeetupId -> AppM [AT.Event]
eventsList meetupId = do
  events <- runDb $ P.selectList [EventMeetupId P.==. meetupId] []
  forM events renderEvent

eventInfo :: EventId -> AppM AT.Event
eventInfo eventId = do
  event <- mustFindM $ runDb $ P.get eventId
  renderEvent (Entity eventId event)

accountInfoEndpoint :: P.Entity User -> AppM AT.AccountInfo
accountInfoEndpoint user = do
  renderAccountInfo user

publicAccountInfo :: P.Entity User -> UserId -> AppM AT.PublicAccountInfo
publicAccountInfo _user userId =
  sg $ do
    user <- mustFindM $ runDb $ P.get userId
    renderPublicAccountInfo (Entity userId user)

logInSendPasswordEndpoint :: AT.LogInSendPasswordForm -> AppM ()
logInSendPasswordEndpoint AT.LogInSendPasswordForm {..} = do
  code <-
    liftIO $
      (S.toText . map Char.toUpper)
        <$> RandomStrings.randomString
          (RandomStrings.onlyAlphaNum RandomStrings.randomASCII)
          5
  logInfo $
    "Generated code for email: " <> display lisEmail <> "; code: "
      <> display code
  t <- liftIO $ getCurrentTime
  _ <-
    runDb $
      P.insert LoginCode
        { loginCodeCode = code,
          loginCodeCreatedAt = t,
          loginCodeEmail = lisEmail
        }
  Le.Emails.sendEmail
    Le.Emails.signInCodeTitle
    (Le.Emails.signInCodeTxt code)
    (Le.Emails.signInCodeHtml code)
    lisEmail
  return ()

logInSendCodeEndpoint ::
  AT.LogInSendCodeForm ->
  AppM (Headers '[Header "Set-Cookie" Cookie.SetCookie] AT.AccountInfo)
logInSendCodeEndpoint AT.LogInSendCodeForm {..} = do
  mLoginCode <-
    runDb $
      P.selectFirst
        [LoginCodeCode P.==. licCode, LoginCodeEmail P.==. licEmail]
        []
  case mLoginCode of
    Nothing -> noHeader <$> formErrors [("code", badCode)]
    Just loginCode -> do
      t <- liftIO getCurrentTime
      if addUTCTime
        Q.loginCodeExpirationPeriod
        (loginCodeCreatedAt (P.entityVal loginCode))
        > t
        then do
          tval <- liftIO $ UUID.toText <$> UUID4.nextRandom
          user <-
            runDb $ do
              user <- blGetOrCreateByEmail t licEmail
              _ <-
                P.insert LoginToken
                  { loginTokenTokenVal = pack (tval),
                    loginTokenUserId = P.entityKey user,
                    loginTokenCreatedAt = t
                  }
              pure user
          let cookie =
                Cookie.defaultSetCookie
                  { Cookie.setCookieName = "u",
                    Cookie.setCookieValue = S.fromText tval,
                    Cookie.setCookiePath = Just "/"
                  }
          accInfo <- renderAccountInfo user
          return $ addHeader cookie accInfo
        else noHeader <$> formErrors [("code", badCode)]
  where
    badCode = "Wrong sign-in code"

logOut ::
  HasCallStack => AppM (Headers '[Header "Set-Cookie" Cookie.SetCookie] ())
logOut =
  sg $ do
    t <- liftIO getCurrentTime
    let back = addUTCTime (-1000000) t
    let cookie =
          ( Cookie.defaultSetCookie
              { Cookie.setCookieName = "u",
                Cookie.setCookieValue = "",
                Cookie.setCookieExpires = Just back,
                Cookie.setCookiePath = Just "/"
              }
          )
    throwM $
      err302
        { errHeaders =
            [ ( "Set-Cookie",
                S.toStrictByteString
                  ( Data.Binary.Builder.toLazyByteString
                      (Cookie.renderSetCookie cookie)
                  )
              ),
              ("Location", "/")
            ]
        }

updateAccount :: P.Entity User -> AT.UpdateAccountForm -> AppM AT.AccountInfo
updateAccount user AT.UpdateAccountForm {..} = do
  user2 <-
    runDb $
      P.updateGet
        (P.entityKey user)
        [ UserFullName P.=. Just (pack uafFullName),
          UserPhoneNumber P.=. Just (pack uafPhoneNumber)
        ]
  accountInfoEndpoint (P.Entity (P.entityKey user) user2)

rsvpList :: EventId -> AppM [AT.RsvpInfo]
rsvpList eventId = do
  _event <- mustFindM $ runDb $ P.getEntity eventId
  rsvpEnts <- runDb $ P.selectList [RsvpEventId P.==. eventId] []
  let rsvps = map P.entityVal rsvpEnts
  let userKeys = map rsvpUserId rsvps
  usersMap <- runDb $ P.getMany userKeys
  return $ mapMaybe (conv usersMap) rsvps
  where
    conv usersMap rsvp = do
      user <- Map.lookup (rsvpUserId rsvp) usersMap
      Just
        ( AT.RsvpInfo
            { rsvName = fromMaybe "" (unpack <$> (userFullName user)),
              rsvUserId = rsvpUserId rsvp
            }
        )

rsvpCreate :: P.Entity User -> EventId -> AppM AT.RsvpInfo
rsvpCreate user eventId = do
  event <- mustFindM $ runDb $ P.getEntity eventId
  mRsvpExisting <-
    runDb $
      P.selectFirst
        [RsvpUserId P.==. P.entityKey user, RsvpEventId P.==. eventId]
        []
  case mRsvpExisting of
    Just _ -> do
      pure ()
    Nothing -> do
      _ <-
        runDb
          $ P.insert
          $ Rsvp {rsvpUserId = P.entityKey user, rsvpEventId = P.entityKey event}
      pure ()
  return $
    AT.RsvpInfo
      { rsvName = fromMaybe "" (unpack <$> userFullName (P.entityVal user)),
        rsvUserId = P.entityKey user
      }

rsvpDelete :: P.Entity User -> EventId -> AppM ()
rsvpDelete user eventId = do
  runDb $
    P.deleteWhere [RsvpEventId P.==. eventId, RsvpUserId P.==. P.entityKey user]
  return ()

attendedMeetups :: P.Entity User -> AppM [AT.AccountAttendedMeetup]
attendedMeetups user = do
  t <- liftIO $ getCurrentTime
  meetups <- runDb $ Q.queryAttendedMeetups user
  forM meetups $ \meetup -> do
    let meetupInf = renderMeetup meetup
    events <- runDb $ Q.queryAttendedEvents user meetup
    eventsRendered <- forM events renderEvent
    meetupComplete <- runDb $ Q.queryIsMeetupComplete t meetup
    pure $
      AT.AccountAttendedMeetup
        { atmMeetup = meetupInf,
          atmEvents = eventsRendered,
          atmComplete = meetupComplete
        }

-- | TODO: check that user attended this meetup's events or was
-- allowed to participate once we have premoderation
meetupFeedbackForms :: P.Entity User -> MeetupId -> AppM [AT.FeedbackForm]
meetupFeedbackForms _user meetupId = do
  t <- liftIO $ getCurrentTime
  feedbackForms <-
    runDb $
      P.selectList
        [FeedbackFormMeetupId P.==. meetupId, FeedbackFormAllowFillSince P.<=. t]
        []
  forM feedbackForms renderFeedbackForm

-- | TODO: restrict users from accessing certain forms
feedbackFormInfo :: P.Entity User -> FeedbackFormId -> AppM AT.FeedbackForm
feedbackFormInfo _user feedbackFormId = do
  feedbackForm <- mustFindM $ runDb $ P.get feedbackFormId
  renderFeedbackForm (P.Entity feedbackFormId feedbackForm)

feedbackGet :: P.Entity User -> FeedbackFormId -> AppM AT.Feedback
feedbackGet user feedbackFormId = do
  mfeedback <-
    runDb $
      P.selectFirst
        [ FeedbackUserId P.==. entityKey user,
          FeedbackFeedbackFormId P.==. feedbackFormId
        ]
        []
  case mfeedback of
    Nothing -> pure emptyFeedback
    Just feedback -> renderFeedback feedback

renderFeedback :: P.Entity Feedback -> AppM AT.Feedback
renderFeedback feedback =
  pure $
    AT.Feedback
      { fdbContent = feedbackContent (entityVal feedback),
        fdbComplete = feedbackComplete (entityVal feedback)
      }

emptyFeedback :: AT.Feedback
emptyFeedback = AT.Feedback {fdbContent = "", fdbComplete = True}

feedbackSet ::
  P.Entity User -> FeedbackFormId -> AT.UpdateFeedback -> AppM AT.Feedback
feedbackSet user feedbackFormId AT.UpdateFeedback {..} = do
  mfeedback <-
    runDb $
      P.selectFirst
        [ FeedbackUserId P.==. entityKey user,
          FeedbackFeedbackFormId P.==. feedbackFormId
        ]
        []
  case mfeedback of
    Nothing -> do
      let feedback =
            Feedback
              { feedbackFeedbackFormId = feedbackFormId,
                feedbackUserId = entityKey user,
                feedbackContent = ufbContent,
                feedbackComplete = ufbComplete
              }
      feedbackId <- runDb $ P.insert $ feedback
      renderFeedback (P.Entity feedbackId feedback)
    Just feedback -> do
      let newFeedback =
            Feedback
              { feedbackFeedbackFormId = feedbackFormId,
                feedbackUserId = entityKey user,
                feedbackContent = ufbContent,
                feedbackComplete = ufbComplete
              }
      runDb $ P.repsert (entityKey feedback) newFeedback
      renderFeedback (P.Entity (entityKey feedback) newFeedback)

lookupAccount ::
  Pool P.SqlBackend -> LoginTokenVal -> Servant.Handler (P.Entity User)
lookupAccount db tok = do
  mToken <- liftIO $ flip P.runSqlPool db $ Q.lookupUserByToken tok
  case mToken of
    Nothing -> throwError (err403 {errBody = "Invalid Token"})
    Just user -> return user

authHandler :: Pool P.SqlBackend -> AuthHandler Request (P.Entity User)
authHandler db = mkAuthHandler handler
  where
    maybeToEither e = maybe (Left e) Right
    throw401 msg = throwError $ err401 {errBody = msg}
    handler req =
      either throw401 (lookupAccount db . pack . toText) $ do
        cookie <-
          maybeToEither "Missing cookie header"
            $ lookup "cookie"
            $ requestHeaders req
        maybeToEither "Missing token in cookie"
          $ lookup "u"
          $ Cookie.parseCookies cookie

applicationForm :: Entity User -> MeetupId -> AppM AT.ApplicationForm
applicationForm _user meetupId =
  sg $ do
    aforms <- runDb $ P.selectList [ApplicationFormMeetupId P.==. meetupId] []
    case aforms of
      [aform] -> do
        renderApplicationForm aform
      _ -> notFound

applicationFieldSave ::
  Entity User ->
  ApplicationFieldId ->
  AT.ApplicationFieldUpdateForm ->
  AppM ()
applicationFieldSave user fieldId AT.ApplicationFieldUpdateForm {..} =
  sg $ do
    fields <-
      runDb $
        P.selectList
          [ UserApplicationFieldFieldId P.==. fieldId,
            UserApplicationFieldUserId P.==. entityKey user
          ]
          []
    let v =
          UserApplicationField
            { userApplicationFieldFieldId = fieldId,
              userApplicationFieldUserId = entityKey user,
              userApplicationFieldContent = afuContent
            }
    case fields of
      [field] -> runDb $ P.repsert (entityKey field) v
      [] -> void $ runDb $ P.insert $ v
      _ -> error "impossible!"

applicationFormSubmit :: Entity User -> ApplicationFormId -> AppM ()
applicationFormSubmit user formId =
  sg $ do
    void
      $ runDb
      $ P.insert
      $ UserApplication
        { userApplicationFormId = formId,
          userApplicationUserId = entityKey user
        }

applicationFormDelete :: Entity User -> MeetupId -> AppM ()
applicationFormDelete user meetupId =
  sg $ do
    forms <- runDb $ P.selectList [ApplicationFormMeetupId P.==. meetupId] []
    runDb $
      P.deleteWhere
        [ UserApplicationUserId P.==. entityKey user,
          UserApplicationFormId P.<-. map entityKey forms
        ]
    pure ()

userApplicationField ::
  Entity User -> ApplicationFieldId -> AppM (Maybe AT.UserApplicationField)
userApplicationField user applicationFieldId =
  sg $ do
    mufield <-
      runDb $
        P.selectFirst
          [ UserApplicationFieldFieldId P.==. applicationFieldId,
            UserApplicationFieldUserId P.==. entityKey user
          ]
          []
    case mufield of
      Nothing -> pure Nothing
      Just ufield -> do
        pure
          $ Just
          $ AT.UserApplicationField
            { uapId = entityKey ufield,
              uapFieldId = applicationFieldId,
              uapContent = userApplicationFieldContent (ev ufield)
            }

userMeetups :: Entity User -> AppM [AT.Meetup]
userMeetups user =
  sg $ do
    meetups <- runDb $ P.selectList [MeetupOrganizerId P.==. entityKey user] []
    pure $ map renderMeetup meetups

userMeetupInfo :: Entity User -> MeetupId -> AppM AT.UserMeetupInfo
userMeetupInfo user meetupId =
  sg $ do
    meetup <- mustFindM $ runDb $ P.get meetupId
    ensureManagesMeetup user (Entity meetupId meetup)
    renderUserMeetupInfo (Entity meetupId meetup)

userMeetupUpsert ::
  Entity User ->
  Maybe MeetupId ->
  AT.MeetupUpdateForm ->
  AppM AT.UserMeetupInfo
userMeetupUpsert user mMeetupId AT.MeetupUpdateForm {..} =
  sg $ do
    mMeetup <-
      case mMeetupId of
        Nothing -> pure Nothing
        Just meetupId -> do
          meetup <- mustFindM $ runDb $ P.get meetupId
          ensureManagesMeetup user (Entity meetupId meetup)
          pure $ Just meetup
    meetupEditRes <-
      runValidateT $ do
        timezone <- attachField "time_zone" $ parseTZName mufTimeZone
        pure $
          case mMeetup of
            Nothing ->
              Meetup
                { meetupTitle = mufTitle,
                  meetupDescription = mufDescription,
                  meetupOrganizerId = entityKey user,
                  meetupMemberApprovalRequired = Just mufMemberApprovalRequired,
                  meetupTimeZone = Just timezone
                }
            Just meetup ->
              meetup
                { meetupTitle = mufTitle,
                  meetupDescription = mufDescription,
                  meetupMemberApprovalRequired = Just mufMemberApprovalRequired,
                  meetupTimeZone = Just timezone
                }
    withFormRes meetupEditRes $ \meetupEdited -> do
      meetupId <-
        case mMeetupId of
          Nothing -> runDb $ P.insert $ meetupEdited
          Just meetupId -> do
            runDb $ P.replace meetupId meetupEdited
            pure meetupId
      meetup2 <- mustFindME $ runDb $ P.get meetupId
      renderUserMeetupInfo (Entity meetupId meetup2)

userMeetupCreate :: Entity User -> AT.MeetupUpdateForm -> AppM AT.UserMeetupInfo
userMeetupCreate user form = userMeetupUpsert user Nothing form

userMeetupUpdate ::
  Entity User -> MeetupId -> AT.MeetupUpdateForm -> AppM AT.UserMeetupInfo
userMeetupUpdate user meetupId form = userMeetupUpsert user (Just meetupId) form

userMeetupApplications :: Entity User -> MeetupId -> AppM [AT.UserApplication]
userMeetupApplications user meetupId =
  sg $ do
    meetup <- mustFindM $ runDb $ P.get meetupId
    ensureManagesMeetup user (Entity meetupId meetup)
    mform <- runDb $ blGetMeetupApplicationForm meetupId
    userApplications <-
      case mform of
        Nothing -> pure []
        Just form ->
          fmap (takeFirstUniqueBy (userApplicationUserId . ev))
            $ runDb
            $ P.selectList
              [UserApplicationFormId P.==. entityKey form]
              [P.Desc UserApplicationId]
    forM userApplications renderUserApplication

userMeetupApplicationAccept :: Entity User -> UserApplicationId -> AppM ()
userMeetupApplicationAccept owner userAppId =
  sg $ do
    userApplication <- mustFindM $ runDb $ P.get userAppId
    let formId = userApplicationFormId userApplication
    let userId = userApplicationUserId userApplication
    user <- mustFindME $ runDb $ P.get userId
    form <- mustFindME $ runDb $ P.get formId
    let meetupId = applicationFormMeetupId form
    meetup <- mustFindME $ runDb $ P.get meetupId
    ensureManagesMeetup owner (Entity meetupId meetup)
    _approvedMemberId <-
      runDb
        $ P.insert
        $ ApprovedMember
          { approvedMemberMeetupId = meetupId,
            approvedMemberUserId = userId,
            approvedMemberStatus = ApprovalStatusApproved
          }
    let meetupName = meetupTitle meetup
        meetupLink = Le.Links.routeMeetup meetupId
    Le.Emails.sendEmail
      Le.Emails.applicationAcceptedTitle
      (Le.Emails.applicationAcceptedTxt meetupName meetupLink)
      (Le.Emails.applicationAcceptedHtml meetupName meetupLink)
      (userEmail user)
    pure ()

userMeetupApplicationReject :: Entity User -> UserApplicationId -> AppM ()
userMeetupApplicationReject owner userAppId =
  sg $ do
    userApplication <- mustFindM $ runDb $ P.get userAppId
    let formId = userApplicationFormId userApplication
    let userId = userApplicationUserId userApplication
    user <- mustFindME $ runDb $ P.get userId
    form <- mustFindME $ runDb $ P.get formId
    let meetupId = applicationFormMeetupId form
    meetup <- mustFindME $ runDb $ P.get meetupId
    ensureManagesMeetup owner (Entity meetupId meetup)
    _approvedMemberId <-
      runDb
        $ P.insert
        $ ApprovedMember
          { approvedMemberMeetupId = meetupId,
            approvedMemberUserId = userId,
            approvedMemberStatus = ApprovalStatusDeclined
          }
    let meetupName = meetupTitle meetup
    Le.Emails.sendEmail
      Le.Emails.applicationRejectedTitle
      (Le.Emails.applicationRejectedTxt meetupName)
      (Le.Emails.applicationRejectedHtml meetupName)
      (userEmail user)
    pure ()

acceptedMembers :: MeetupId -> AppM [AT.PublicAccountInfo]
acceptedMembers meetupId =
  sg $ do
    members <-
      runDb $
        P.selectList
          [ ApprovedMemberMeetupId P.==. meetupId,
            ApprovedMemberStatus P.==. ApprovalStatusApproved
          ]
          []
    forM members $ \member -> do
      let userId = approvedMemberUserId (ev member)
      user <- mustFindME $ runDb $ P.get userId
      renderPublicAccountInfo (Entity userId user)

userEventUpsert ::
  Entity User -> Maybe EventId -> AT.EventUpdateForm -> AppM AT.Event
userEventUpsert user mEventId AT.EventUpdateForm {..} =
  sg $ do
    meetup <-
      case mEventId of
        Nothing -> do
          let meetupId = eufMeetupId
          meetup <- mustFindME $ runDb $ P.get meetupId
          ensureManagesMeetup user (Entity meetupId meetup)
          pure meetup
        Just eventId -> do
          event <- mustFindM $ runDb $ P.get eventId
          let meetupId = eventMeetupId event
          meetup <- mustFindME $ runDb $ P.get meetupId
          ensureManagesMeetup user (Entity meetupId meetup)
          pure meetup
    let tz =
          Data.Time.Zones.All.tzByLabel
            (fromMaybe Data.Time.Zones.All.Etc__UTC (meetupTimeZone meetup))
    eventEditRes <-
      runValidateT $ do
        loc <-
          attachField "location_link" $
            case eufLocationLink of
              "" -> pure Nothing
              _ -> Just <$> parseUrl eufLocationLink
        pure $
          Event
            { eventMeetupId = eufMeetupId,
              eventDate = zonedTimeToUTC (millisecondsToZonedTime tz eufDate),
              eventLocation = (Just eufLocation),
              eventLocationLink = loc
            }
    withFormRes eventEditRes $ \eventEdited -> do
      eventId <-
        case mEventId of
          Nothing -> runDb $ P.insert $ eventEdited
          Just eventId -> do
            runDb $ P.replace eventId eventEdited
            pure eventId
      event2 <- mustFindME $ runDb $ P.get eventId
      renderEvent (Entity eventId event2)

userEventCreate :: Entity User -> AT.EventUpdateForm -> AppM AT.Event
userEventCreate user form = userEventUpsert user Nothing form

userEventUpdate :: Entity User -> EventId -> AT.EventUpdateForm -> AppM AT.Event
userEventUpdate user eventId form = userEventUpsert user (Just eventId) form
