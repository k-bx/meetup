{-# LANGUAGE QuasiQuotes #-}

module Le.Emails where

import qualified Data.String.Class as S
import Le.Import
import Le.Types
import qualified Mail.Hailgun as Hailgun
import Servant
import Text.InterpolatedString.Perl6 (qc)

sendEmail :: Hailgun.MessageSubject -> Text -> Text -> Text -> AppM ()
sendEmail title txt html emailTo = do
  env <- ask
  let ctx =
        Hailgun.HailgunContext
          { hailgunDomain = S.toString (cfgMailgunDomain (envConfig env))
          , hailgunApiKey = S.toString (cfgMailgunApiKey (envConfig env))
          , hailgunProxy = Nothing
          }
  let hailgunMsg =
        Hailgun.hailgunMessage
          title
          (Hailgun.TextAndHTML (toBS txt) (toBS html))
          "meetup.events.info@gmail.com"
          (Hailgun.MessageRecipients
             { recipientsTo = [S.fromText emailTo]
             , recipientsCC = []
             , recipientsBCC = []
             })
          []
  case hailgunMsg of
    Left err -> do
      let e = "Error while constructing Hailgun email: " <> (fromString err)
      logError e
      throwM $ err500 {errBody = S.toLazyByteString (utf8BuilderToText e)}
    Right msg -> do
      liftIO (Hailgun.sendEmail ctx msg) >>= \case
        Left err -> do
          let e =
                "Error while sending Hailgun email: " <>
                display (S.toText (Hailgun.herMessage err))
          logError e
          throwM $ err500 {errBody = S.toLazyByteString (utf8BuilderToText e)}
        Right sendRsp -> do
          logInfo $
            "Email sent. Mailgun response: " <>
            fromString (Hailgun.hsrMessage sendRsp)

signInCodeTitle :: Text
signInCodeTitle = "Meetup Sign-In Link"

signInCodeTxt :: Text -> Text
signInCodeTxt code =
  [qc|
          Meowdy, partner

Your sign-in code is: {code}

All the best
|]

signInCodeHtml :: Text -> Text
signInCodeHtml code =
  [qc|Meowdy, partner<br><br>

Your sign-in code is: {code}<br><br>

All the best
|]

applicationAcceptedTitle :: Text
applicationAcceptedTitle = [qc|Your application has been accepted|]

applicationAcceptedTxt :: Text -> Text -> Text
applicationAcceptedTxt meetupName meetupLink =
  [qc|Greetings!

We are happy to inform you that your application for {meetupName} has been accepted!

Feel free to RSVP at its events now at {meetupLink}

All best
|]

applicationAcceptedHtml :: Text -> Text -> Text
applicationAcceptedHtml meetupName meetupLink =
  [qc|Greetings!<br><br>

We are happy to inform you that your application for {meetupName} has been accepted!<br><br>

Feel free to RSVP at its events now at {meetupLink}<br><br>

All best
|]

applicationRejectedTitle :: Text
applicationRejectedTitle = [qc|Your application has been rejected|]

applicationRejectedTxt :: Text -> Text
applicationRejectedTxt meetupName =
  [qc|Greetings!

We are sorry to inform you, but your application for a meetup "{meetupName}" has been rejected.

Please contact event organizers for more info.

All best
|]

applicationRejectedHtml :: Text -> Text
applicationRejectedHtml meetupName =
  [qc|Greetings!<br><br>

We are sorry to inform you, but your application for a meetup "{meetupName}" has been rejected.<br><br>

Please contact event organizers for more info.<br><br>

All best
|]
