module Le.AppUtils where

import qualified Data.String.Class as S
import qualified Database.Persist.Postgresql as P
import GHC.Stack
import Le.Import
import Le.Types
import Servant

runDb :: (MonadReader Env m, MonadIO m) => ReaderT P.SqlBackend IO b -> m b
runDb f = do
  pool <- asks envDb
  liftIO $ flip P.runSqlPool pool $ f

-- | Safeguard. Good for GHC HasCallStack
sg :: (MonadUnliftIO m, HasCallStack) => m a -> m a
sg act = catchAny act onErr
  where
    onErr e = do
      liftIO $
        S.hPutStrLn stderr $
        "Error: Safeguard caught an exception: " <> tshow e <> ". Call stack: " <>
        S.toText (prettyCallStack callStack)
      liftIO $ throwIO e

mustFind :: Maybe a -> AppM a
mustFind = maybe notFound pure

mustFindM :: AppM (Maybe a) -> AppM a
mustFindM = (=<<) mustFind

-- | Logs error
mustFindE :: HasCallStack => Maybe a -> AppM a
mustFindE = maybe (error "Couldn't find object") pure

-- | Logs error
mustFindME :: HasCallStack => AppM (Maybe a) -> AppM a
mustFindME = (=<<) mustFindE

-- | Logs error via `error`
mustFindErr :: (HasCallStack, Monad m) => Maybe a -> m a
mustFindErr = maybe (error "Couldn't find object.") pure

-- | Logs error via `error`
mustFindMErr :: (HasCallStack, Monad m) => m (Maybe a) -> m a
mustFindMErr = (=<<) mustFindErr

notFound :: AppM a
notFound = throwM err404

assertM :: HasCallStack => Bool -> AppM ()
assertM x =
  case x of
    False -> error "assertion error"
    True -> pure ()
