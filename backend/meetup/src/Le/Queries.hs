module Le.Queries where

import Data.Time.Clock (NominalDiffTime, UTCTime, addUTCTime, getCurrentTime)
import Database.Esqueleto
import Le.Model
import Le.Types
import RIO hiding ((^.), isNothing, on)

lookupUserByToken ::
     MonadIO m => LoginTokenVal -> ReaderT SqlBackend m (Maybe (Entity User))
lookupUserByToken tokenVal = do
  t <- liftIO getCurrentTime
  mToken <- getBy (LoginTokenQuery tokenVal)
  case mToken of
    Nothing -> return Nothing
    Just tok ->
      if addUTCTime tokenExpirationPeriod (loginTokenCreatedAt (entityVal tok)) >
         t
        then getEntity (loginTokenUserId (entityVal tok))
        else return Nothing

latestMeetups :: MonadIO m => ReaderT SqlBackend m [Entity Meetup]
latestMeetups = do
  latestMeetupIds <-
    select $
    from $ \event -> do
      orderBy [desc (event ^. EventDate)]
      limit 100
      return $ event ^. EventMeetupId
  meetups <-
    select $
    from $ \meetup -> do
      where_ $ meetup ^. MeetupId `in_` valList (map unValue latestMeetupIds)
      return meetup
  return meetups

queryAttendedMeetups ::
     MonadIO m => Entity User -> ReaderT SqlBackend m [Entity Meetup]
queryAttendedMeetups user = do
  select $
    from $ \(meetup `InnerJoin` event `InnerJoin` rsvp) -> do
      on (event ^. EventId ==. rsvp ^. RsvpEventId)
      on (meetup ^. MeetupId ==. event ^. EventMeetupId)
      where_ (rsvp ^. RsvpUserId ==. val (entityKey user))
      limit 100
      groupBy (meetup ^. MeetupId)
      return $ meetup

queryAttendedEvents ::
     MonadIO m
  => Entity User
  -> Entity Meetup
  -> ReaderT SqlBackend m [Entity Event]
queryAttendedEvents user meetup = do
  select $
    from $ \(event `InnerJoin` rsvp) -> do
      on (event ^. EventId ==. rsvp ^. RsvpEventId)
      where_
        ((rsvp ^. RsvpUserId ==. val (entityKey user)) &&.
         (event ^. EventMeetupId ==. val (entityKey meetup)))
      groupBy (event ^. EventId)
      return $ event

queryIsMeetupComplete ::
     MonadIO m => UTCTime -> Entity Meetup -> ReaderT SqlBackend m Bool
queryIsMeetupComplete t meetup = do
  evs <-
    select $
    from $ \(m `InnerJoin` event) -> do
      on (m ^. MeetupId ==. event ^. EventMeetupId)
      where_ (event ^. EventMeetupId ==. val (entityKey meetup))
      orderBy [desc (event ^. EventDate)]
      limit 1
      return $ event
  case evs of
    [] -> pure True
    (event:_) -> do
      if t > eventDate (entityVal event)
        then pure True
        else pure False

-- TODO: move out in Constants.hs
tokenExpirationPeriod :: NominalDiffTime
tokenExpirationPeriod = 60 * 60 * 24 * 10

loginCodeExpirationPeriod :: NominalDiffTime
loginCodeExpirationPeriod = 60 * 10
