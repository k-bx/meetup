module Le.Links where

import qualified Database.Persist.Postgresql as P
-- | TODO: re-make into a type-safe form
import Le.Import
import Le.Model

absoluteLinkBase :: Text
absoluteLinkBase = "https://meetup.events"

routeMeetup :: MeetupId -> Text
routeMeetup meetupId = absoluteLinkBase <> "/meetup/" <> tshow @Int64 (P.fromSqlKey meetupId)
