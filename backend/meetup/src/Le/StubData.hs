module Le.StubData where

import Control.Newtype
import qualified Data.Maybe as Maybe
import Data.Pool (Pool)
import Data.Time.Clock (getCurrentTime)
import qualified Data.Time.Zones.All
import qualified Database.Persist.Postgresql as P
import Le.Handlers
import Le.Model
import Network.URI (parseAbsoluteURI)
import Prelude
import RIO

dropDbData :: Pool P.SqlBackend -> RIO RioApp ()
dropDbData pool = do
  flip P.runSqlPool pool $ do
    P.deleteWhere ([] :: [P.Filter Event])
    P.deleteWhere ([] :: [P.Filter Meetup])
    P.deleteWhere ([] :: [P.Filter User])
    P.deleteWhere ([] :: [P.Filter Rsvp])

insertStubData :: Pool P.SqlBackend -> RIO RioApp ()
insertStubData pool = do
  flip P.runSqlPool pool $ do
    t <- liftIO getCurrentTime
    let meetupUser =
          User
            { userEmail = "k-bx@k-bx.com"
            , userPhoneNumber = Just $ pack "+380731787269"
            , userFullName = Just $ pack "Meetup"
            , userAvatar_300 = Nothing
            , userAvatarOrig = Nothing
            , userAdmin = True
            , userCreatedAt = t
            , userUpdatedAt = t
            }
    userId <- P.insert meetupUser
    let meetup01 =
          Meetup
            { meetupTitle = "Elm Study Group"
            , meetupDescription =
                "Join us at [Elm Study Group](https://github.com/KyivHaskell/elm-study-group) meetup"
            , meetupOrganizerId = userId
            , meetupMemberApprovalRequired = Nothing
            , meetupTimeZone = Just Data.Time.Zones.All.Europe__Kiev
            }
    meetupId01 <- P.insert meetup01
    let event01 =
          Event
            { eventMeetupId = meetupId01
            , eventDate = Prelude.read "2019-02-28 19:00:00"
            , eventLocation = Just "Working Cow Cafe"
            , eventLocationLink =
                Just $
                pack
                  (Maybe.fromJust
                     (parseAbsoluteURI "https://goo.gl/maps/DwbuXZo1Epw"))
            }
    _eventId01 <- P.insert event01
    return ()
