module Le.Utils where

import qualified Data.Map.Strict as M
import qualified Data.String.Class as S
import RIO
import Safe

readEitherSafeText :: Read a => Text -> Either Text a
readEitherSafeText t =
  case readEitherSafe (S.toString t) of
    Left s -> Left (S.toText s)
    Right r -> Right r

takeFirstUniqueBy :: Ord key => (item -> key) -> [item] -> [item]
takeFirstUniqueBy f entities = go (M.fromList []) entities
  where
    go _c [] = []
    go c (x:xs) =
      case M.lookup (f x) c of
        Nothing -> x : go (M.insert (f x) x c) xs
        Just _ -> go c xs
