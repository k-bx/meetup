module Le.Validators where

import Le.Types
import qualified Data.Text as T
import Network.URI as URI
import qualified Data.String.Class as S
import qualified Data.Time.Zones.All
import Le.Import

validateList ::
     Applicative m
  => Text
  -> (t -> ValidateT b1 m b)
  -> [t]
  -> ValidateT [(Text, b1)] m [b]
validateList prefix validator xs =
  traverse
    (\(i, x) -> attachField (prefix <> tshow @Int i) (validator x))
    (zip [0 ..] xs)

mapFailure :: (e1 -> e2) -> Validation e1 a -> Validation e2 a
mapFailure f x =
  case x of
    Failure e1 -> Failure (f e1)
    Success s -> Success s

mapFailureValidateT ::
     Functor m => (b0 -> b1) -> ValidateT b0 m a0 -> ValidateT b1 m a0
mapFailureValidateT f c = bimapValidateT f id c

attachField ::
     Functor m => Text -> ValidateT b1 m b2 -> ValidateT [(Text, b1)] m b2
attachField fld = bimapValidateT (\v -> [(fld, v)]) id

valIsJust :: Applicative m => Maybe a -> ValidateT Text m a
valIsJust ma =
  case ma of
    Nothing -> throwVT "This field is mandatory."
    Just v -> pure v

notEmpty :: Applicative m => Text -> ValidateT Text m Text
notEmpty txt =
  if T.null txt
    then throwVT "This field cannot be empty."
    else pure txt

parseUrl :: Applicative m => Text -> ValidateT Text m PersistentAbsoluteURI
parseUrl txt =
  case URI.parseAbsoluteURI (S.toString txt) of
    Nothing -> throwVT $ "Not a valid url: " <> txt
    Just u -> pure (pack u)

parseTZName ::
     Applicative m
  => Text
  -> ValidateT Text m Data.Time.Zones.All.TZLabel
parseTZName name =
  case Data.Time.Zones.All.fromTZName (S.fromText name) of
    Nothing -> throwVT $ "Couldn't parse time zone: " <> name
    Just v -> pure v
