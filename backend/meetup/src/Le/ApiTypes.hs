{-# LANGUAGE TemplateHaskell #-}

module Le.ApiTypes where

import Elm.Derive
import Le.Aeson
import qualified Le.Model as M
import Network.URI (URI)
import RIO

type IntUTCTime = Int

-- | See Note [TimeZone Conversion] and Note [TimeZone and Wire]
type IntZonedTime = Int

data Meetup =
  Meetup
    { meeId :: M.MeetupId
    , meeTitle :: Text
    , meeDescription :: Text
    , meeOrganizerId :: M.UserId
    , meeMemberApprovalRequired :: Bool
    , meeTimeZone :: Text
    }
  deriving (Show, Eq, Generic)

data Event =
  Event
    { evtId :: M.EventId
    , evtDate :: IntZonedTime
    , evtLocation :: Maybe Text
    , evtLocationLink :: Maybe Text
    , evtMeetupId :: M.MeetupId
    }
  deriving (Show, Eq, Generic)

data AccountInfo =
  AccountInfo
    { accId :: M.UserId
    , accEmail :: Text
    , accFullName :: Maybe Text
    , accPhoneNumber :: Maybe Text
    , accInfoComplete :: Bool
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''AccountInfo

data PublicAccountInfo =
  PublicAccountInfo
    { paiId :: M.UserId
    , paiFullName :: Maybe Text
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''PublicAccountInfo

data LogInSendPasswordForm =
  LogInSendPasswordForm
    { lisEmail :: Text
    }
  deriving (Show, Eq, Generic)

data LogInSendCodeForm =
  LogInSendCodeForm
    { licEmail :: Text
    , licCode :: Text
    }
  deriving (Show, Eq, Generic)

data UpdateAccountForm =
  UpdateAccountForm
    { uafFullName :: Text
    , uafPhoneNumber :: Text
    }
  deriving (Show, Eq, Generic)

data RsvpInfo =
  RsvpInfo
    { rsvName :: Text
    , rsvUserId :: M.UserId
    }
  deriving (Show, Eq, Generic)

data AccountAttendedMeetup =
  AccountAttendedMeetup
    { atmMeetup :: Meetup
    , atmEvents :: [Event]
    , atmComplete :: Bool
    }

data FeedbackForm =
  FeedbackForm
    { ffmId :: M.FeedbackFormId
    , ffmAllowFillSince :: Int
    , ffmTitle :: Text
    , ffmDescription :: Text
    }

data Feedback =
  Feedback
    { fdbContent :: Text
    , fdbComplete :: Bool
    }
  deriving (Show, Eq, Generic)

data UpdateFeedback =
  UpdateFeedback
    { ufbContent :: Text
    , ufbComplete :: Bool
    }
  deriving (Show, Eq, Generic)

-- | Don't use the constructor directly, convert from URI by using 'mkJsonURI'
newtype JsonURI =
  JsonURIDangerConstructor Text
  deriving (Show, Eq, Generic)

unJsonURI :: JsonURI -> Text
unJsonURI (JsonURIDangerConstructor x) = x

mkJsonURI :: URI -> JsonURI
mkJsonURI u = JsonURIDangerConstructor (tshow u)

deriveBoth (jsonOpts 0) ''JsonURI

deriveBoth (jsonOpts 3) ''Meetup

deriveBoth (jsonOpts 3) ''Event

deriveBoth (jsonOpts 3) ''LogInSendPasswordForm

deriveBoth (jsonOpts 3) ''LogInSendCodeForm

deriveBoth (jsonOpts 3) ''UpdateAccountForm

deriveBoth (jsonOpts 3) ''RsvpInfo

deriveBoth (jsonOpts 3) ''AccountAttendedMeetup

deriveBoth (jsonOpts 3) ''FeedbackForm

deriveBoth (jsonOpts 3) ''Feedback

deriveBoth (jsonOpts 3) ''UpdateFeedback

data ApplicationField =
  ApplicationField
    { afdFormId :: M.ApplicationFormId
    , afdId :: M.ApplicationFieldId
    , afdDescription :: Text
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''ApplicationField

data ApplicationForm =
  ApplicationForm
    { afiId :: M.ApplicationFormId
    , afiMeetupId :: M.MeetupId
    , afiDescription :: Text
    , afiFields :: [ApplicationField]
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''ApplicationForm

data MeetupAuthInfo =
  MeetupAuthInfo
    { maiId :: M.MeetupId
    , maiApprovedMemberId :: Maybe M.ApprovedMemberId
    , maiUserApplicationId :: Maybe M.UserApplicationId
    , maiMembershipApproved :: Maybe Bool
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''MeetupAuthInfo

data ApplicationFieldUpdateForm =
  ApplicationFieldUpdateForm
    { afuContent :: Text
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''ApplicationFieldUpdateForm

data UserApplicationField =
  UserApplicationField
    { uapId :: M.UserApplicationFieldId
    , uapFieldId :: M.ApplicationFieldId
    , uapContent :: Text
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''UserApplicationField

data UserMeetupInfo =
  UserMeetupInfo
    { umiId :: M.MeetupId
    , umiTitle :: Text
    , umiDescription :: Text
    , umiOrganizerId :: M.UserId
    , umiMemberApprovalRequired :: Bool
    , umiTimeZone :: Text
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''UserMeetupInfo

data MeetupUpdateForm =
  MeetupUpdateForm
    { mufTitle :: Text
    , mufDescription :: Text
    , mufMemberApprovalRequired :: Bool
    , mufTimeZone :: Text
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''MeetupUpdateForm

data UserApplication =
  UserApplication
    { ualId :: M.UserApplicationId
    , ualFields :: [UserApplicationField]
    , ualUserId :: M.UserId
    , ualMembershipApproved :: Maybe Bool
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''UserApplication

data EventUpdateForm =
  EventUpdateForm
    { eufDate :: IntZonedTime
    , eufLocation :: Text
    , eufLocationLink :: Text
    , eufMeetupId :: M.MeetupId
    }
  deriving (Show, Eq, Generic)

deriveBoth (jsonOpts 3) ''EventUpdateForm
