module Le.Import
  ( module X
  , parseAbsoluteURI
  , ev
  , toBS
  , fromBS
  , toLBS
  , fromLBS
  ) where

import Control.Applicative as X
import Control.Lens as X ((.~))
import Control.Newtype as X hiding (over)
import qualified Data.ByteString.Lazy as BL
import Data.Either.Validation as X (Validation(..))
import Data.Either.ValidationT as X
  ( ValidateT
  , bimapValidateT
  , runValidateT
  , throwVT
  )
import Data.Monoid as X (Sum(..))
import Data.Pool as X (Pool)
import Data.Scientific as X (Scientific)
import Data.String.Class as X (ConvString, toStrictByteString, toString, toText)
import qualified Data.String.Class as S
import Data.Time as X (defaultTimeLocale, formatTime)
import Data.Time.Additional as X
import Data.Time.Clock as X
  ( NominalDiffTime
  , UTCTime
  , addUTCTime
  , getCurrentTime
  )
import Data.Time.LocalTime as X (LocalTime, ZonedTime, zonedTimeToUTC)
import Data.Time.Zones as X (TZ)
import Data.Time.Zones.All as X (TZLabel, tzByLabel)
import Data.Void as X
import Database.Persist.Postgresql as X (Entity(..), entityKey, entityVal)
import Le.Aeson as X (jsonOpts)
import Le.Utils as X
import Network.HTTP.Types.Status as X (notFound404, ok200)
import Network.URI as X (URI(..), URIAuth(..))
import qualified Network.URI as URI
import RIO as X
import Servant.API as X (FromHttpApiData(..), ToHttpApiData(..))
import Web.PathPieces as X (PathPiece(..))

parseAbsoluteURI :: ConvString s => s -> Maybe URI
parseAbsoluteURI = URI.parseAbsoluteURI . toString

ev :: Entity record -> record
ev = entityVal

toBS :: S.ConvStrictByteString s => s -> ByteString
toBS = S.toStrictByteString

fromBS :: S.ConvStrictByteString s => ByteString -> s
fromBS = S.fromStrictByteString

toLBS :: S.ConvLazyByteString s => s -> BL.ByteString
toLBS = S.toLazyByteString

fromLBS :: S.ConvLazyByteString s => BL.ByteString -> s
fromLBS = S.fromLazyByteString
