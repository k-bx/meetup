module Le.Handlers.Common where

import qualified Data.Aeson as J
import Le.Import
import Le.Model
import Le.Types
import Servant

ensureManagesMeetup :: Entity User -> Entity Meetup -> AppM ()
ensureManagesMeetup user meetup = do
  case (meetupOrganizerId (ev meetup) == entityKey user) of
    False -> error "Meetup not managed by a user"
    True -> pure ()

formErrors :: [(Text, Text)] -> AppM a
formErrors xs = throwM err400 {errBody = J.encode xs}

withFormRes :: Validation [(Text, Text)] t -> (t -> AppM a) -> AppM a
withFormRes res f =
  case res of
    Success v -> f v
    Failure errs -> formErrors errs
