module Le.BusinessLogic where

import qualified Database.Persist.Postgresql as P
import Le.Import
import Le.Model

blGetOrCreateByEmail ::
     MonadIO m => UTCTime -> Text -> ReaderT P.SqlBackend m (P.Entity User)
blGetOrCreateByEmail t email = do
  mUser <- P.selectFirst [UserEmail P.==. email] []
  case mUser of
    Nothing -> do
      userKey <-
        P.insert
          User
            { userEmail = email
            , userPhoneNumber = Nothing
            , userFullName = Nothing
            , userAvatar_300 = Nothing
            , userAvatarOrig = Nothing
            , userAdmin = False
            , userCreatedAt = t
            , userUpdatedAt = t
            }
      P.getJustEntity userKey
    Just user -> pure user

-- | Currently it is assumed that a meetup has at most one application form
blGetMeetupApplicationForm ::
     MonadIO m
  => MeetupId
  -> ReaderT P.SqlBackend m (Maybe (P.Entity ApplicationForm))
blGetMeetupApplicationForm meetupId =
  P.selectFirst [ApplicationFormMeetupId P.==. meetupId] []
