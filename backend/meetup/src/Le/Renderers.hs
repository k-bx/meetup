module Le.Renderers where

import qualified Data.Time.Zones.All
import qualified Database.Persist.Postgresql as P
import qualified Le.ApiTypes as AT
import Le.AppUtils
import Le.Import
import qualified Data.String.Class as S
import Le.Model
import Le.Types

renderAccountInfo :: Entity User -> AppM AT.AccountInfo
renderAccountInfo user = do
  pure $
    AT.AccountInfo
      { accId = entityKey user
      , accEmail = userEmail (ev user)
      , accInfoComplete = isInfoComplete (ev user)
      , accFullName = unpack <$> userFullName (ev user)
      , accPhoneNumber = unpack <$> userPhoneNumber (ev user)
      }

isInfoComplete :: User -> Bool
isInfoComplete user =
  if isEmpty (fmap (unpack :: FullName -> Text) (userFullName user)) ||
     isEmpty (fmap (unpack :: PhoneNumber -> Text) (userPhoneNumber user))
    then False
    else True
  where
    isEmpty Nothing = True
    isEmpty (Just "") = True
    isEmpty _ = False

renderMeetup :: P.Entity Meetup -> AT.Meetup
renderMeetup meetup =
  AT.Meetup
    { meeId = entityKey meetup
    , meeTitle = meetupTitle (ev meetup)
    , meeDescription = meetupDescription (ev meetup)
    , meeOrganizerId = meetupOrganizerId (ev meetup)
    , meeMemberApprovalRequired =
        fromMaybe False (meetupMemberApprovalRequired (ev meetup))
    , meeTimeZone = case meetupTimeZone (ev meetup) of
        Nothing -> ""
        Just tzLabel -> S.toText (Data.Time.Zones.All.toTZName tzLabel)
    }

renderUserMeetupInfo :: P.Entity Meetup -> AppM AT.UserMeetupInfo
renderUserMeetupInfo meetup =
  pure $
  AT.UserMeetupInfo
    { umiId = entityKey meetup
    , umiTitle = meetupTitle (ev meetup)
    , umiDescription = meetupDescription (ev meetup)
    , umiOrganizerId = meetupOrganizerId (ev meetup)
    , umiMemberApprovalRequired =
        fromMaybe False (meetupMemberApprovalRequired (ev meetup))
    , umiTimeZone = case meetupTimeZone (ev meetup) of
        Nothing -> ""
        Just tzLabel -> S.toText (Data.Time.Zones.All.toTZName tzLabel)
    }

renderEvent :: P.Entity Event -> AppM AT.Event
renderEvent event = do
  meetup <- mustFindME $ runDb $ P.get (eventMeetupId (ev event))
  let tz = fromMaybe Data.Time.Zones.All.Etc__UTC (meetupTimeZone meetup)
  pure $
    AT.Event
      { evtId = entityKey event
      , evtDate = zonedTimeToMilliseconds (utcToZonedTime' (tzByLabel tz) (eventDate (ev event)))
      , evtLocation = eventLocation (ev event)
      , evtLocationLink =
          (tshow @URI . unpack) <$> (eventLocationLink (ev event))
      , evtMeetupId = eventMeetupId (ev event)
      }

renderFeedbackForm :: P.Entity FeedbackForm -> AppM AT.FeedbackForm
renderFeedbackForm feedbackForm =
  pure $
  AT.FeedbackForm
    { ffmId = entityKey feedbackForm
    , ffmAllowFillSince =
        utcTimeToMilliseconds
          (feedbackFormAllowFillSince (entityVal feedbackForm))
    , ffmTitle = feedbackFormTitle (entityVal feedbackForm)
    , ffmDescription = feedbackFormDescription (entityVal feedbackForm)
    }

renderApplicationField :: Entity ApplicationField -> AppM AT.ApplicationField
renderApplicationField field = do
  pure $
    AT.ApplicationField
      { afdFormId = applicationFieldFormId (ev field)
      , afdId = entityKey field
      , afdDescription = applicationFieldDescription (ev field)
      }

renderApplicationForm :: Entity ApplicationForm -> AppM AT.ApplicationForm
renderApplicationForm form = do
  afields <-
    runDb $ P.selectList [ApplicationFieldFormId P.==. entityKey form] []
  afieldsR <- mapM renderApplicationField afields
  pure $
    AT.ApplicationForm
      { afiId = entityKey form
      , afiMeetupId = applicationFormMeetupId (ev form)
      , afiDescription = applicationFormDescription (ev form)
      , afiFields = afieldsR
      }

renderUserApplicationField :: Entity UserApplicationField -> AppM AT.UserApplicationField
renderUserApplicationField userAppField = do
  pure $
    AT.UserApplicationField
      { uapId = entityKey userAppField
      , uapFieldId = userApplicationFieldFieldId (ev userAppField)
      , uapContent = userApplicationFieldContent (ev userAppField)
      }

renderUserApplication :: Entity UserApplication -> AppM AT.UserApplication
renderUserApplication userApp = do
  let formId = userApplicationFormId (ev userApp)
  let userId = userApplicationUserId (ev userApp)
  applicationFields <-
    runDb $ P.selectList [ApplicationFieldFormId P.==. formId] []
  let applicationFieldsIds = map entityKey applicationFields
  userApplicationFields <-
    runDb $
    P.selectList
      [ UserApplicationFieldFieldId P.<-. applicationFieldsIds
      , UserApplicationFieldUserId P.==. userId
      ]
      []
  uafields <- forM userApplicationFields renderUserApplicationField
  applicationForm <- mustFindME $ runDb $ P.get formId
  let meetupId = applicationFormMeetupId applicationForm
  mApprovedMember <-
    runDb $
    P.selectFirst
      [ApprovedMemberMeetupId P.==. meetupId, ApprovedMemberUserId P.==. userId]
      []
  let membershipApproved =
        case mApprovedMember of
          Nothing -> Nothing
          Just x ->
            case approvedMemberStatus (ev x) of
              ApprovalStatusApproved -> Just True
              ApprovalStatusDeclined -> Just False
  pure $
    AT.UserApplication
      { ualId = entityKey userApp
      , ualFields = uafields
      , ualUserId = userApplicationUserId (ev userApp)
      , ualMembershipApproved = membershipApproved
      }

renderPublicAccountInfo :: Entity User -> AppM AT.PublicAccountInfo
renderPublicAccountInfo user =
  pure $
  AT.PublicAccountInfo
    {paiId = entityKey user, paiFullName = unpack <$> userFullName (ev user)}
