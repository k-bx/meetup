#!/usr/bin/env stack
-- stack --resolver=lts-14.6 script
{-# LANGUAGE OverloadedStrings #-}

-- | Develop with:
--     
--     cd backend/meetup
--     ghcid --command="stack ghci --ghc-options='-Wall' ./../../sysadmin/fix-letsencrypt.hs"
--
-- Add "--run" to run on save (be careful!)
--
import Control.Monad (forM)
import qualified Data.String.Class as S
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Time
import qualified Data.Time.Format
import qualified System.Directory
import System.Process.Typed

getStamp :: IO String
getStamp = do
  t <- Data.Time.getCurrentTime
  let stamp =
        Data.Time.Format.formatTime Data.Time.Format.defaultTimeLocale "%F-%X" t
  pure stamp

backupFile :: FilePath -> IO ()
backupFile fpath = do
  home <- System.Directory.getHomeDirectory
  stamp <- getStamp
  System.Directory.copyFile
    fpath
    (S.toString
       (home <> "/tmp/" <> (S.toString (T.replace "/" "_" (S.toText fpath))) <>
        "-" <>
        stamp))

envvarsFileReplaceValue :: FilePath -> T.Text -> T.Text -> IO ()
envvarsFileReplaceValue fpath kName kNewVal = do
  backupFile fpath
  contents <- T.readFile fpath
  lines2 <-
    forM (T.lines contents) $ \line -> do
      case (T.splitOn " " line) of
        ["export", kv] -> do
          let [kRaw, vRaw] = T.splitOn "=" kv
          let k = T.strip kRaw
          let v = T.strip vRaw
          let v2 =
                case k == kName of
                  True -> kNewVal
                  False -> v
          pure $ "export " <> k <> "=" <> v2
        _ -> error "Expecting \"export k=v\" line syntax"
  T.writeFile fpath (T.unlines lines2)

main :: IO ()
main = do
  home <- System.Directory.getHomeDirectory
  runProcess_ "ssh ubuntu@meetup sudo systemctl stop meetup.service"
  runProcess_
    "ssh ubuntu@meetup sudo certbot certonly --force-renew -d meetup.events"
  -- TODO: put "1<RET>" in stdout
  (ls, _) <- readProcess_ "ssh ubuntu@meetup sudo ls -l /etc/letsencrypt/keys/"
  let fname = last (T.splitOn " " (S.toText (last (T.lines (S.toText ls)))))
  print fname
  let fnameFull = "/etc/letsencrypt/keys/" <> fname
  envvarsFileReplaceValue (home <> "/Dropbox/gfhjkb/meetup/envvars_prod") "MEETUP_KEY" fnameFull
  runProcess_ (setWorkingDir ".." "make sysadmin-setup")
  -- TODO: do something more lightweight? Even better: rewrite in Haskell intellectually
  runProcess_ "ssh ubuntu@meetup sudo systemctl start meetup.service"
