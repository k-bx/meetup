#!/usr/bin/env stack
{- stack script
     --resolver=lts-13.24
     --package rio
     --package text
     --package string-class
     --package fsnotify
     --package directory
     --package process
     --package typed-process
-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wall -Werror #-}

module Main where

import qualified Control.Concurrent.MVar as MVar
import qualified Data.String.Class as S
import qualified Data.Text as T
import RIO
import System.Directory (canonicalizePath)
import System.Exit (ExitCode(..))
import qualified System.FSNotify as FSNotify
import System.Process.Typed

makeDef, makeElm, makeSass, launch :: ProcessConfig () () ()
makeDef = "make fast"

makeElm = "(cd frontend && make) && make post"

makeSass = "(cd frontend && make sass) && make post"

launch = proc "./meetup" ["webapp"]

main :: IO ()
main = do
  lastSignalMV <- MVar.newEmptyMVar
  forever $ do
    mLastSignal <- MVar.tryReadMVar lastSignalMV
    putStrLn ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    c <-
      case mLastSignal of
        Nothing -> do
          putStrLn $ ">> NOTHING"
          runProcess makeDef
        Just ev ->
          case getEvPath ev of
            Nothing -> do
              putStrLn $ ">> PATH IS NOTHING"
              runProcess makeDef
            Just path ->
              let x' = S.toText path
               in if ".elm" `T.isSuffixOf` x'
                    then do
                      putStrLn $ ">> elm"
                      runProcess makeElm
                    else if ".scss" `T.isSuffixOf` x'
                           then do
                             putStrLn $ ">> sass"
                             runProcess makeSass
                           else do
                             putStrLn $ ">> NOMATCH"
                             runProcess makeDef
    mWebApp <-
      case c of
        ExitSuccess -> do
          Just <$>
            startProcess
              (setWorkingDir "dist" launch)
        _ -> pure Nothing
    putStrLn $ ">> mWebApp: " ++ show mWebApp
    (mynotifywait lastSignalMV) >> threadDelay 10000 -- 10 ms
    case mWebApp of
      Just webApp -> do
        putStrLn ">> Killing the webapp...."
        stopProcess webApp
      Nothing -> pure ()
    putStrLn "==================================================="

getEvPath :: FSNotify.Event -> Maybe FilePath
getEvPath ev =
  case ev of
    FSNotify.Modified mpath _t _bool -> Just mpath
    FSNotify.Added mpath _t _bool -> Just mpath
    FSNotify.Unknown mpath _t _bool -> Just mpath
    _ -> Nothing

-- | Similar to inoitfywait but hardcoded for our setup and is
-- cross-platform (should work on macOS). Waits for files to change
-- (excluding some) and then exits.
mynotifywait :: MVar FSNotify.Event -> IO ()
mynotifywait lastSignal = do
  dir <- canonicalizePath "."
  exit <- MVar.newEmptyMVar
  exit2 <- MVar.newEmptyMVar
  let pathMatches x =
        let x' = S.toText x
         in if (S.toText dir) `T.isPrefixOf` x' &&
               not ("#" `T.isInfixOf` x') &&
               not (".git" `T.isInfixOf` x') &&
               not (".stack-work" `T.isInfixOf` x')
              then True
              else False
  putStrLn $ "> Listening to directory: " <> dir
  _ <-
    FSNotify.withManager $ \mgr -> do
      let predicate ev =
            case getEvPath ev of
              Nothing -> False
              Just path -> pathMatches path
      _ <-
        FSNotify.watchTree mgr dir predicate $ \ev -> do
          putStrLn $ "> FSNotify signal: " <> show ev
          MVar.isEmptyMVar lastSignal >>= \case
            False -> void $ swapMVar lastSignal ev
            True -> putMVar lastSignal ev
          MVar.putMVar exit2 ()
          MVar.putMVar exit ()
      MVar.takeMVar exit2
  MVar.takeMVar exit
