window.mainInit = null;

(function() {
    class MarkdownEditor extends HTMLElement {
        connectedCallback() {
            var input = document.createElement("textarea");
            this.appendChild(input);
            var simplemde = new SimpleMDE({ element: input, spellChecker: false });
            simplemde.value(this.val);
            var markdownNode = this;
            simplemde.codemirror.on("change", function() {
                let v = simplemde.value()
                var event = new CustomEvent("md-input", {detail: {content: v}});
                markdownNode.dispatchEvent(event);
            });
        }
    }

    window.customElements.define("markdown-editor", MarkdownEditor);

    window.mainInit = function() {
        var app = Elm.Index.init({node: document.getElementById("app")});
        app.ports.needLoginRedirect.subscribe(function(data) {
            let loc = window.location.href;
            window.localStorage.setItem('afterSigninRedirectUrl', loc);
            window.location.href = '/login';
        });
        app.ports.redirectBackAfterLogin.subscribe(function(data) {
            try {
                let loc = window.localStorage.getItem('afterSigninRedirectUrl');
                if (loc) {
                    window.localStorage.removeItem('afterSigninRedirectUrl');
                    window.location.href = loc;
                } else {
                    window.location.href = '/';
                }
            } catch (error) {
                reportError(error);
            }
        });
    }
})();
