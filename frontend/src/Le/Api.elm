module Le.Api exposing (..)

import Http
import Json.Decode exposing (Value)
import Json.Encode
import Json.Helpers exposing (required, fnullable, maybeEncode, decodeSumUnaries)
import Dict exposing (Dict)
import Url.Builder

type alias Text = String
jsonDecText = Json.Decode.string

type alias Tuple2 a b = { t2f1 : a, t2f2 : b }

jsonDecTuple2 : Json.Decode.Decoder dec1 -> Json.Decode.Decoder dec2 -> Json.Decode.Decoder (Tuple2 dec1 dec2)
jsonDecTuple2 dec1 dec2 =
   Json.Decode.succeed (\pv1 pv2 -> {t2f1 = pv1, t2f2 = pv2})
   |> required "t2f1" dec1
   |> required "t2f2" dec2

jsonPair : Json.Decode.Decoder a -> Json.Decode.Decoder b -> Json.Decode.Decoder ( a, b )
jsonPair a b =
    Json.Decode.andThen
        (\xs ->
            case xs of
                [] ->
                    Json.Decode.fail "Expecting a list of two elements"

                x :: y :: [] ->
                    case ( Json.Decode.decodeValue a x, Json.Decode.decodeValue b y ) of
                        ( Ok av, Ok bv ) ->
                            Json.Decode.succeed ( av, bv )

                        _ ->
                            Json.Decode.fail "Error while decoding individual pair fields"

                _ ->
                    Json.Decode.fail "Expecting a list of two elements"
        )
        (Json.Decode.list Json.Decode.value)

type alias Error =
    { httpError : Http.Error
    , formErrors : Dict String String
    , errors : List String
    }

leExpectJson : (Result Error a -> msg) -> Json.Decode.Decoder a -> Http.Expect msg
leExpectJson toMsg decoder =
    Http.expectStringResponse toMsg <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err { httpError = Http.BadUrl url, formErrors = Dict.empty, errors = [] }

                Http.Timeout_ ->
                    Err { httpError = Http.Timeout, formErrors = Dict.empty, errors = [] }

                Http.NetworkError_ ->
                    Err { httpError = Http.NetworkError, formErrors = Dict.empty, errors = [] }

                Http.BadStatus_ metadata body ->
                    case metadata.statusCode of
                        400 ->
                            case Json.Decode.decodeString (Json.Decode.list (jsonPair Json.Decode.string Json.Decode.string)) body of
                                Err decodeErr ->
                                    Err
                                        { httpError = Http.BadStatus metadata.statusCode
                                        , errors = [ "Error while decoding a back-end response: " ++ Json.Decode.errorToString decodeErr ]
                                        , formErrors = Dict.empty
                                        }

                                Ok vs ->
                                    Err
                                        { httpError = Http.BadStatus metadata.statusCode
                                        , errors = []
                                        , formErrors = Dict.fromList vs
                                        }

                        _ ->
                            Err { httpError = Http.BadStatus metadata.statusCode, formErrors = Dict.empty, errors = ["There was a back-end error. Try again or contact administrators"] }

                Http.GoodStatus_ metadata body ->
                    case Json.Decode.decodeString decoder body of
                        Ok value ->
                            Ok value

                        Err err ->
                            Err { httpError = Http.BadBody (Json.Decode.errorToString err), formErrors = Dict.empty, errors = [] }

type alias JsonURI  = String

jsonDecJsonURI : Json.Decode.Decoder ( JsonURI )
jsonDecJsonURI =
    Json.Decode.string

jsonEncJsonURI : JsonURI -> Value
jsonEncJsonURI  val = Json.Encode.string val



type Condition  =
    New 
    | LikeNew 
    | VeryGood 
    | Good 
    | Acceptable 
    | BetterThanNothing 

jsonDecCondition : Json.Decode.Decoder ( Condition )
jsonDecCondition = 
    let jsonDecDictCondition = Dict.fromList [("New", New), ("LikeNew", LikeNew), ("VeryGood", VeryGood), ("Good", Good), ("Acceptable", Acceptable), ("BetterThanNothing", BetterThanNothing)]
    in  decodeSumUnaries "Condition" jsonDecDictCondition

jsonEncCondition : Condition -> Value
jsonEncCondition  val =
    case val of
        New -> Json.Encode.string "New"
        LikeNew -> Json.Encode.string "LikeNew"
        VeryGood -> Json.Encode.string "VeryGood"
        Good -> Json.Encode.string "Good"
        Acceptable -> Json.Encode.string "Acceptable"
        BetterThanNothing -> Json.Encode.string "BetterThanNothing"



type alias Meetup  =
   { id: MeetupId
   , title: String
   , description: String
   , organizer_id: UserId
   , member_approval_required: Bool
   , time_zone: String
   }

jsonDecMeetup : Json.Decode.Decoder ( Meetup )
jsonDecMeetup =
   Json.Decode.succeed (\pid ptitle pdescription porganizer_id pmember_approval_required ptime_zone -> {id = pid, title = ptitle, description = pdescription, organizer_id = porganizer_id, member_approval_required = pmember_approval_required, time_zone = ptime_zone})
   |> required "id" (jsonDecMeetupId)
   |> required "title" (Json.Decode.string)
   |> required "description" (Json.Decode.string)
   |> required "organizer_id" (jsonDecUserId)
   |> required "member_approval_required" (Json.Decode.bool)
   |> required "time_zone" (Json.Decode.string)

jsonEncMeetup : Meetup -> Value
jsonEncMeetup  val =
   Json.Encode.object
   [ ("id", jsonEncMeetupId val.id)
   , ("title", Json.Encode.string val.title)
   , ("description", Json.Encode.string val.description)
   , ("organizer_id", jsonEncUserId val.organizer_id)
   , ("member_approval_required", Json.Encode.bool val.member_approval_required)
   , ("time_zone", Json.Encode.string val.time_zone)
   ]



type alias UserMeetupInfo  =
   { id: MeetupId
   , title: String
   , description: String
   , organizer_id: UserId
   , member_approval_required: Bool
   , time_zone: String
   }

jsonDecUserMeetupInfo : Json.Decode.Decoder ( UserMeetupInfo )
jsonDecUserMeetupInfo =
   Json.Decode.succeed (\pid ptitle pdescription porganizer_id pmember_approval_required ptime_zone -> {id = pid, title = ptitle, description = pdescription, organizer_id = porganizer_id, member_approval_required = pmember_approval_required, time_zone = ptime_zone})
   |> required "id" (jsonDecMeetupId)
   |> required "title" (Json.Decode.string)
   |> required "description" (Json.Decode.string)
   |> required "organizer_id" (jsonDecUserId)
   |> required "member_approval_required" (Json.Decode.bool)
   |> required "time_zone" (Json.Decode.string)

jsonEncUserMeetupInfo : UserMeetupInfo -> Value
jsonEncUserMeetupInfo  val =
   Json.Encode.object
   [ ("id", jsonEncMeetupId val.id)
   , ("title", Json.Encode.string val.title)
   , ("description", Json.Encode.string val.description)
   , ("organizer_id", jsonEncUserId val.organizer_id)
   , ("member_approval_required", Json.Encode.bool val.member_approval_required)
   , ("time_zone", Json.Encode.string val.time_zone)
   ]



type alias MeetupAuthInfo  =
   { id: MeetupId
   , approved_member_id: (Maybe ApprovedMemberId)
   , user_application_id: (Maybe UserApplicationId)
   , membership_approved: (Maybe Bool)
   }

jsonDecMeetupAuthInfo : Json.Decode.Decoder ( MeetupAuthInfo )
jsonDecMeetupAuthInfo =
   Json.Decode.succeed (\pid papproved_member_id puser_application_id pmembership_approved -> {id = pid, approved_member_id = papproved_member_id, user_application_id = puser_application_id, membership_approved = pmembership_approved})
   |> required "id" (jsonDecMeetupId)
   |> fnullable "approved_member_id" (jsonDecApprovedMemberId)
   |> fnullable "user_application_id" (jsonDecUserApplicationId)
   |> fnullable "membership_approved" (Json.Decode.bool)

jsonEncMeetupAuthInfo : MeetupAuthInfo -> Value
jsonEncMeetupAuthInfo  val =
   Json.Encode.object
   [ ("id", jsonEncMeetupId val.id)
   , ("approved_member_id", (maybeEncode (jsonEncApprovedMemberId)) val.approved_member_id)
   , ("user_application_id", (maybeEncode (jsonEncUserApplicationId)) val.user_application_id)
   , ("membership_approved", (maybeEncode (Json.Encode.bool)) val.membership_approved)
   ]



type alias Event  =
   { id: EventId
   , date: IntZonedTime
   , location: (Maybe String)
   , location_link: (Maybe String)
   , meetup_id: MeetupId
   }

jsonDecEvent : Json.Decode.Decoder ( Event )
jsonDecEvent =
   Json.Decode.succeed (\pid pdate plocation plocation_link pmeetup_id -> {id = pid, date = pdate, location = plocation, location_link = plocation_link, meetup_id = pmeetup_id})
   |> required "id" (jsonDecEventId)
   |> required "date" (jsonDecIntZonedTime)
   |> fnullable "location" (Json.Decode.string)
   |> fnullable "location_link" (Json.Decode.string)
   |> required "meetup_id" (jsonDecMeetupId)

jsonEncEvent : Event -> Value
jsonEncEvent  val =
   Json.Encode.object
   [ ("id", jsonEncEventId val.id)
   , ("date", jsonEncIntZonedTime val.date)
   , ("location", (maybeEncode (Json.Encode.string)) val.location)
   , ("location_link", (maybeEncode (Json.Encode.string)) val.location_link)
   , ("meetup_id", jsonEncMeetupId val.meetup_id)
   ]



type alias AccountInfo  =
   { id: UserId
   , email: String
   , full_name: (Maybe String)
   , phone_number: (Maybe String)
   , info_complete: Bool
   }

jsonDecAccountInfo : Json.Decode.Decoder ( AccountInfo )
jsonDecAccountInfo =
   Json.Decode.succeed (\pid pemail pfull_name pphone_number pinfo_complete -> {id = pid, email = pemail, full_name = pfull_name, phone_number = pphone_number, info_complete = pinfo_complete})
   |> required "id" (jsonDecUserId)
   |> required "email" (Json.Decode.string)
   |> fnullable "full_name" (Json.Decode.string)
   |> fnullable "phone_number" (Json.Decode.string)
   |> required "info_complete" (Json.Decode.bool)

jsonEncAccountInfo : AccountInfo -> Value
jsonEncAccountInfo  val =
   Json.Encode.object
   [ ("id", jsonEncUserId val.id)
   , ("email", Json.Encode.string val.email)
   , ("full_name", (maybeEncode (Json.Encode.string)) val.full_name)
   , ("phone_number", (maybeEncode (Json.Encode.string)) val.phone_number)
   , ("info_complete", Json.Encode.bool val.info_complete)
   ]



type alias PublicAccountInfo  =
   { id: UserId
   , full_name: (Maybe String)
   }

jsonDecPublicAccountInfo : Json.Decode.Decoder ( PublicAccountInfo )
jsonDecPublicAccountInfo =
   Json.Decode.succeed (\pid pfull_name -> {id = pid, full_name = pfull_name})
   |> required "id" (jsonDecUserId)
   |> fnullable "full_name" (Json.Decode.string)

jsonEncPublicAccountInfo : PublicAccountInfo -> Value
jsonEncPublicAccountInfo  val =
   Json.Encode.object
   [ ("id", jsonEncUserId val.id)
   , ("full_name", (maybeEncode (Json.Encode.string)) val.full_name)
   ]



type alias LogInSendPasswordForm  =
   { email: String
   }

jsonDecLogInSendPasswordForm : Json.Decode.Decoder ( LogInSendPasswordForm )
jsonDecLogInSendPasswordForm =
   Json.Decode.succeed (\pemail -> {email = pemail})
   |> required "email" (Json.Decode.string)

jsonEncLogInSendPasswordForm : LogInSendPasswordForm -> Value
jsonEncLogInSendPasswordForm  val =
   Json.Encode.object
   [ ("email", Json.Encode.string val.email)
   ]



type alias LogInSendCodeForm  =
   { email: String
   , code: String
   }

jsonDecLogInSendCodeForm : Json.Decode.Decoder ( LogInSendCodeForm )
jsonDecLogInSendCodeForm =
   Json.Decode.succeed (\pemail pcode -> {email = pemail, code = pcode})
   |> required "email" (Json.Decode.string)
   |> required "code" (Json.Decode.string)

jsonEncLogInSendCodeForm : LogInSendCodeForm -> Value
jsonEncLogInSendCodeForm  val =
   Json.Encode.object
   [ ("email", Json.Encode.string val.email)
   , ("code", Json.Encode.string val.code)
   ]



type alias UpdateAccountForm  =
   { full_name: String
   , phone_number: String
   }

jsonDecUpdateAccountForm : Json.Decode.Decoder ( UpdateAccountForm )
jsonDecUpdateAccountForm =
   Json.Decode.succeed (\pfull_name pphone_number -> {full_name = pfull_name, phone_number = pphone_number})
   |> required "full_name" (Json.Decode.string)
   |> required "phone_number" (Json.Decode.string)

jsonEncUpdateAccountForm : UpdateAccountForm -> Value
jsonEncUpdateAccountForm  val =
   Json.Encode.object
   [ ("full_name", Json.Encode.string val.full_name)
   , ("phone_number", Json.Encode.string val.phone_number)
   ]



type alias RsvpInfo  =
   { name: String
   , user_id: UserId
   }

jsonDecRsvpInfo : Json.Decode.Decoder ( RsvpInfo )
jsonDecRsvpInfo =
   Json.Decode.succeed (\pname puser_id -> {name = pname, user_id = puser_id})
   |> required "name" (Json.Decode.string)
   |> required "user_id" (jsonDecUserId)

jsonEncRsvpInfo : RsvpInfo -> Value
jsonEncRsvpInfo  val =
   Json.Encode.object
   [ ("name", Json.Encode.string val.name)
   , ("user_id", jsonEncUserId val.user_id)
   ]



type alias AccountAttendedMeetup  =
   { meetup: Meetup
   , events: (List Event)
   , complete: Bool
   }

jsonDecAccountAttendedMeetup : Json.Decode.Decoder ( AccountAttendedMeetup )
jsonDecAccountAttendedMeetup =
   Json.Decode.succeed (\pmeetup pevents pcomplete -> {meetup = pmeetup, events = pevents, complete = pcomplete})
   |> required "meetup" (jsonDecMeetup)
   |> required "events" (Json.Decode.list (jsonDecEvent))
   |> required "complete" (Json.Decode.bool)

jsonEncAccountAttendedMeetup : AccountAttendedMeetup -> Value
jsonEncAccountAttendedMeetup  val =
   Json.Encode.object
   [ ("meetup", jsonEncMeetup val.meetup)
   , ("events", (Json.Encode.list jsonEncEvent) val.events)
   , ("complete", Json.Encode.bool val.complete)
   ]



type alias FeedbackForm  =
   { id: FeedbackFormId
   , allow_fill_since: Int
   , title: String
   , description: String
   }

jsonDecFeedbackForm : Json.Decode.Decoder ( FeedbackForm )
jsonDecFeedbackForm =
   Json.Decode.succeed (\pid pallow_fill_since ptitle pdescription -> {id = pid, allow_fill_since = pallow_fill_since, title = ptitle, description = pdescription})
   |> required "id" (jsonDecFeedbackFormId)
   |> required "allow_fill_since" (Json.Decode.int)
   |> required "title" (Json.Decode.string)
   |> required "description" (Json.Decode.string)

jsonEncFeedbackForm : FeedbackForm -> Value
jsonEncFeedbackForm  val =
   Json.Encode.object
   [ ("id", jsonEncFeedbackFormId val.id)
   , ("allow_fill_since", Json.Encode.int val.allow_fill_since)
   , ("title", Json.Encode.string val.title)
   , ("description", Json.Encode.string val.description)
   ]



type alias Feedback  =
   { content: String
   , complete: Bool
   }

jsonDecFeedback : Json.Decode.Decoder ( Feedback )
jsonDecFeedback =
   Json.Decode.succeed (\pcontent pcomplete -> {content = pcontent, complete = pcomplete})
   |> required "content" (Json.Decode.string)
   |> required "complete" (Json.Decode.bool)

jsonEncFeedback : Feedback -> Value
jsonEncFeedback  val =
   Json.Encode.object
   [ ("content", Json.Encode.string val.content)
   , ("complete", Json.Encode.bool val.complete)
   ]



type alias MeetupUpdateForm  =
   { title: String
   , description: String
   , member_approval_required: Bool
   , time_zone: String
   }

jsonDecMeetupUpdateForm : Json.Decode.Decoder ( MeetupUpdateForm )
jsonDecMeetupUpdateForm =
   Json.Decode.succeed (\ptitle pdescription pmember_approval_required ptime_zone -> {title = ptitle, description = pdescription, member_approval_required = pmember_approval_required, time_zone = ptime_zone})
   |> required "title" (Json.Decode.string)
   |> required "description" (Json.Decode.string)
   |> required "member_approval_required" (Json.Decode.bool)
   |> required "time_zone" (Json.Decode.string)

jsonEncMeetupUpdateForm : MeetupUpdateForm -> Value
jsonEncMeetupUpdateForm  val =
   Json.Encode.object
   [ ("title", Json.Encode.string val.title)
   , ("description", Json.Encode.string val.description)
   , ("member_approval_required", Json.Encode.bool val.member_approval_required)
   , ("time_zone", Json.Encode.string val.time_zone)
   ]



type alias UpdateFeedback  =
   { content: String
   , complete: Bool
   }

jsonDecUpdateFeedback : Json.Decode.Decoder ( UpdateFeedback )
jsonDecUpdateFeedback =
   Json.Decode.succeed (\pcontent pcomplete -> {content = pcontent, complete = pcomplete})
   |> required "content" (Json.Decode.string)
   |> required "complete" (Json.Decode.bool)

jsonEncUpdateFeedback : UpdateFeedback -> Value
jsonEncUpdateFeedback  val =
   Json.Encode.object
   [ ("content", Json.Encode.string val.content)
   , ("complete", Json.Encode.bool val.complete)
   ]



type alias ApplicationField  =
   { form_id: ApplicationFormId
   , id: ApplicationFieldId
   , description: String
   }

jsonDecApplicationField : Json.Decode.Decoder ( ApplicationField )
jsonDecApplicationField =
   Json.Decode.succeed (\pform_id pid pdescription -> {form_id = pform_id, id = pid, description = pdescription})
   |> required "form_id" (jsonDecApplicationFormId)
   |> required "id" (jsonDecApplicationFieldId)
   |> required "description" (Json.Decode.string)

jsonEncApplicationField : ApplicationField -> Value
jsonEncApplicationField  val =
   Json.Encode.object
   [ ("form_id", jsonEncApplicationFormId val.form_id)
   , ("id", jsonEncApplicationFieldId val.id)
   , ("description", Json.Encode.string val.description)
   ]



type alias ApplicationForm  =
   { id: ApplicationFormId
   , meetup_id: MeetupId
   , description: String
   , fields: (List ApplicationField)
   }

jsonDecApplicationForm : Json.Decode.Decoder ( ApplicationForm )
jsonDecApplicationForm =
   Json.Decode.succeed (\pid pmeetup_id pdescription pfields -> {id = pid, meetup_id = pmeetup_id, description = pdescription, fields = pfields})
   |> required "id" (jsonDecApplicationFormId)
   |> required "meetup_id" (jsonDecMeetupId)
   |> required "description" (Json.Decode.string)
   |> required "fields" (Json.Decode.list (jsonDecApplicationField))

jsonEncApplicationForm : ApplicationForm -> Value
jsonEncApplicationForm  val =
   Json.Encode.object
   [ ("id", jsonEncApplicationFormId val.id)
   , ("meetup_id", jsonEncMeetupId val.meetup_id)
   , ("description", Json.Encode.string val.description)
   , ("fields", (Json.Encode.list jsonEncApplicationField) val.fields)
   ]



type alias ApplicationFieldUpdateForm  =
   { content: String
   }

jsonDecApplicationFieldUpdateForm : Json.Decode.Decoder ( ApplicationFieldUpdateForm )
jsonDecApplicationFieldUpdateForm =
   Json.Decode.succeed (\pcontent -> {content = pcontent})
   |> required "content" (Json.Decode.string)

jsonEncApplicationFieldUpdateForm : ApplicationFieldUpdateForm -> Value
jsonEncApplicationFieldUpdateForm  val =
   Json.Encode.object
   [ ("content", Json.Encode.string val.content)
   ]



type alias UserApplication  =
   { id: UserApplicationId
   , fields: (List UserApplicationField)
   , user_id: UserId
   , membership_approved: (Maybe Bool)
   }

jsonDecUserApplication : Json.Decode.Decoder ( UserApplication )
jsonDecUserApplication =
   Json.Decode.succeed (\pid pfields puser_id pmembership_approved -> {id = pid, fields = pfields, user_id = puser_id, membership_approved = pmembership_approved})
   |> required "id" (jsonDecUserApplicationId)
   |> required "fields" (Json.Decode.list (jsonDecUserApplicationField))
   |> required "user_id" (jsonDecUserId)
   |> fnullable "membership_approved" (Json.Decode.bool)

jsonEncUserApplication : UserApplication -> Value
jsonEncUserApplication  val =
   Json.Encode.object
   [ ("id", jsonEncUserApplicationId val.id)
   , ("fields", (Json.Encode.list jsonEncUserApplicationField) val.fields)
   , ("user_id", jsonEncUserId val.user_id)
   , ("membership_approved", (maybeEncode (Json.Encode.bool)) val.membership_approved)
   ]



type alias UserApplicationField  =
   { id: UserApplicationFieldId
   , field_id: ApplicationFieldId
   , content: String
   }

jsonDecUserApplicationField : Json.Decode.Decoder ( UserApplicationField )
jsonDecUserApplicationField =
   Json.Decode.succeed (\pid pfield_id pcontent -> {id = pid, field_id = pfield_id, content = pcontent})
   |> required "id" (jsonDecUserApplicationFieldId)
   |> required "field_id" (jsonDecApplicationFieldId)
   |> required "content" (Json.Decode.string)

jsonEncUserApplicationField : UserApplicationField -> Value
jsonEncUserApplicationField  val =
   Json.Encode.object
   [ ("id", jsonEncUserApplicationFieldId val.id)
   , ("field_id", jsonEncApplicationFieldId val.field_id)
   , ("content", Json.Encode.string val.content)
   ]



type alias EventUpdateForm  =
   { date: IntZonedTime
   , location: String
   , location_link: String
   , meetup_id: MeetupId
   }

jsonDecEventUpdateForm : Json.Decode.Decoder ( EventUpdateForm )
jsonDecEventUpdateForm =
   Json.Decode.succeed (\pdate plocation plocation_link pmeetup_id -> {date = pdate, location = plocation, location_link = plocation_link, meetup_id = pmeetup_id})
   |> required "date" (jsonDecIntZonedTime)
   |> required "location" (Json.Decode.string)
   |> required "location_link" (Json.Decode.string)
   |> required "meetup_id" (jsonDecMeetupId)

jsonEncEventUpdateForm : EventUpdateForm -> Value
jsonEncEventUpdateForm  val =
   Json.Encode.object
   [ ("date", jsonEncIntZonedTime val.date)
   , ("location", Json.Encode.string val.location)
   , ("location_link", Json.Encode.string val.location_link)
   , ("meetup_id", jsonEncMeetupId val.meetup_id)
   ]


type alias IntUTCTime = Int
jsonDecIntUTCTime = Json.Decode.int
jsonEncIntUTCTime = Json.Encode.int
type alias IntZonedTime = Int
jsonDecIntZonedTime = Json.Decode.int
jsonEncIntZonedTime = Json.Encode.int
type alias Int64 = Int
jsonDecInt64 = Json.Decode.int
jsonEncInt64 = Json.Encode.int
type alias MeetupId = Int
jsonDecMeetupId = Json.Decode.int
jsonEncMeetupId = Json.Encode.int
type alias EventId = Int
jsonDecEventId = Json.Decode.int
jsonEncEventId = Json.Encode.int
type alias FeedbackFormId = Int
jsonDecFeedbackFormId = Json.Decode.int
jsonEncFeedbackFormId = Json.Encode.int
type alias UserId = Int
jsonDecUserId = Json.Decode.int
jsonEncUserId = Json.Encode.int
type alias ApplicationFieldId = Int
jsonDecApplicationFieldId = Json.Decode.int
jsonEncApplicationFieldId = Json.Encode.int
type alias ApplicationFormId = Int
jsonDecApplicationFormId = Json.Decode.int
jsonEncApplicationFormId = Json.Encode.int
type alias ApprovedMemberId = Int
jsonDecApprovedMemberId = Json.Decode.int
jsonEncApprovedMemberId = Json.Encode.int
type alias UserApplicationId = Int
jsonDecUserApplicationId = Json.Decode.int
jsonEncUserApplicationId = Json.Encode.int
type alias UserApplicationFieldId = Int
jsonDecUserApplicationFieldId = Json.Decode.int
jsonEncUserApplicationFieldId = Json.Encode.int
getApiMeetupsjson : (Result Error  ((List Meetup))  -> msg) -> Cmd msg
getApiMeetupsjson toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetups.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecMeetup))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiMeetupByMeetupidInfojson : MeetupId -> (Result Error  (Meetup)  -> msg) -> Cmd msg
getApiMeetupByMeetupidInfojson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetup"
                    , (capture_meetup_id |> String.fromInt)
                    , "info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecMeetup
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiMeetupByMeetupidAuthinfojson : MeetupId -> (Result Error  (MeetupAuthInfo)  -> msg) -> Cmd msg
getApiMeetupByMeetupidAuthinfojson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetup"
                    , (capture_meetup_id |> String.fromInt)
                    , "auth-info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecMeetupAuthInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiMeetupByMeetupidEventsjson : MeetupId -> (Result Error  ((List Event))  -> msg) -> Cmd msg
getApiMeetupByMeetupidEventsjson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetup"
                    , (capture_meetup_id |> String.fromInt)
                    , "events.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecEvent))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiEventsByEventidInfojson : EventId -> (Result Error  (Event)  -> msg) -> Cmd msg
getApiEventsByEventidInfojson capture_event_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "events"
                    , (capture_event_id |> String.fromInt)
                    , "info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecEvent
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiAccountinfojson : (Result Error  (AccountInfo)  -> msg) -> Cmd msg
getApiAccountinfojson toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "account-info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecAccountInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiUserByUseridPublicaccountinfojson : UserId -> (Result Error  (PublicAccountInfo)  -> msg) -> Cmd msg
getApiUserByUseridPublicaccountinfojson capture_user_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , (capture_user_id |> String.fromInt)
                    , "public-account-info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecPublicAccountInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiLoginsendpassword : LogInSendPasswordForm -> (Result Error  (())  -> msg) -> Cmd msg
postApiLoginsendpassword body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "log-in-send-password"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncLogInSendPasswordForm body)
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiLogin : LogInSendCodeForm -> (Result Error  (AccountInfo)  -> msg) -> Cmd msg
postApiLogin body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "log-in"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncLogInSendCodeForm body)
            , expect =
                leExpectJson toMsg jsonDecAccountInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiLogout : (Result Error  (())  -> msg) -> Cmd msg
getApiLogout toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "log-out"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiAccountUpdatejson : UpdateAccountForm -> (Result Error  (AccountInfo)  -> msg) -> Cmd msg
postApiAccountUpdatejson body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "account"
                    , "update.json"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncUpdateAccountForm body)
            , expect =
                leExpectJson toMsg jsonDecAccountInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiMeetupsAttendedjson : (Result Error  ((List AccountAttendedMeetup))  -> msg) -> Cmd msg
getApiMeetupsAttendedjson toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetups"
                    , "attended.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecAccountAttendedMeetup))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiMeetupsByMeetupidFeedbackformsjson : MeetupId -> (Result Error  ((List FeedbackForm))  -> msg) -> Cmd msg
getApiMeetupsByMeetupidFeedbackformsjson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetups"
                    , (capture_meetup_id |> String.fromInt)
                    , "feedback-forms.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecFeedbackForm))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiFeedbackformsByFeedbackformidInfojson : FeedbackFormId -> (Result Error  (FeedbackForm)  -> msg) -> Cmd msg
getApiFeedbackformsByFeedbackformidInfojson capture_feedback_form_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "feedback-forms"
                    , (capture_feedback_form_id
                       |> String.fromInt)
                    , "info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecFeedbackForm
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiFeedbackformsByFeedbackformidFeedbackjson : FeedbackFormId -> (Result Error  (Feedback)  -> msg) -> Cmd msg
getApiFeedbackformsByFeedbackformidFeedbackjson capture_feedback_form_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "feedback-forms"
                    , (capture_feedback_form_id
                       |> String.fromInt)
                    , "feedback.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecFeedback
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiFeedbackformsByFeedbackformidSetfeedbackjson : FeedbackFormId -> UpdateFeedback -> (Result Error  (Feedback)  -> msg) -> Cmd msg
postApiFeedbackformsByFeedbackformidSetfeedbackjson capture_feedback_form_id body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "feedback-forms"
                    , (capture_feedback_form_id
                       |> String.fromInt)
                    , "set-feedback.json"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncUpdateFeedback body)
            , expect =
                leExpectJson toMsg jsonDecFeedback
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiEventByEventidRsvpListjson : EventId -> (Result Error  ((List RsvpInfo))  -> msg) -> Cmd msg
getApiEventByEventidRsvpListjson capture_event_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "event"
                    , (capture_event_id |> String.fromInt)
                    , "rsvp"
                    , "list.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecRsvpInfo))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiEventByEventidRsvpCreatejson : EventId -> (Result Error  (RsvpInfo)  -> msg) -> Cmd msg
postApiEventByEventidRsvpCreatejson capture_event_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "event"
                    , (capture_event_id |> String.fromInt)
                    , "rsvp"
                    , "create.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecRsvpInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



deleteApiEventByEventidRsvpDeletejson : EventId -> (Result Error  (())  -> msg) -> Cmd msg
deleteApiEventByEventidRsvpDeletejson capture_event_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "DELETE"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "event"
                    , (capture_event_id |> String.fromInt)
                    , "rsvp"
                    , "delete.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiMeetupByMeetupidApplicationformjson : MeetupId -> (Result Error  (ApplicationForm)  -> msg) -> Cmd msg
getApiMeetupByMeetupidApplicationformjson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetup"
                    , (capture_meetup_id |> String.fromInt)
                    , "application-form.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecApplicationForm
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiApplicationfieldByApplicationfieldidUpdatejson : ApplicationFieldId -> ApplicationFieldUpdateForm -> (Result Error  (())  -> msg) -> Cmd msg
postApiApplicationfieldByApplicationfieldidUpdatejson capture_application_field_id body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "application-field"
                    , (capture_application_field_id
                       |> String.fromInt)
                    , "update.json"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncApplicationFieldUpdateForm body)
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiApplicationformByApplicationformidSubmitjson : ApplicationFormId -> (Result Error  (())  -> msg) -> Cmd msg
postApiApplicationformByApplicationformidSubmitjson capture_application_form_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "application-form"
                    , (capture_application_form_id
                       |> String.fromInt)
                    , "submit.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiMeetupByMeetupidApplicationformDeletejson : MeetupId -> (Result Error  (())  -> msg) -> Cmd msg
postApiMeetupByMeetupidApplicationformDeletejson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetup"
                    , (capture_meetup_id |> String.fromInt)
                    , "application-form"
                    , "delete.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiUserapplicationfieldByUserapplicationfieldidInfojson : ApplicationFieldId -> (Result Error  ((Maybe UserApplicationField))  -> msg) -> Cmd msg
getApiUserapplicationfieldByUserapplicationfieldidInfojson capture_user_application_field_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user-application-field"
                    , (capture_user_application_field_id
                       |> String.fromInt)
                    , "info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.maybe (jsonDecUserApplicationField))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiUserMeetupsjson : (Result Error  ((List Meetup))  -> msg) -> Cmd msg
getApiUserMeetupsjson toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "meetups.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecMeetup))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiUserMeetupsByMeetupidInfojson : MeetupId -> (Result Error  (UserMeetupInfo)  -> msg) -> Cmd msg
getApiUserMeetupsByMeetupidInfojson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "meetups"
                    , (capture_meetup_id |> String.fromInt)
                    , "info.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg jsonDecUserMeetupInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiUserMeetupsCreatejson : MeetupUpdateForm -> (Result Error  (UserMeetupInfo)  -> msg) -> Cmd msg
postApiUserMeetupsCreatejson body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "meetups"
                    , "create.json"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncMeetupUpdateForm body)
            , expect =
                leExpectJson toMsg jsonDecUserMeetupInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiUserMeetupsByMeetupidUpdatejson : MeetupId -> MeetupUpdateForm -> (Result Error  (UserMeetupInfo)  -> msg) -> Cmd msg
postApiUserMeetupsByMeetupidUpdatejson capture_meetup_id body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "meetups"
                    , (capture_meetup_id |> String.fromInt)
                    , "update.json"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncMeetupUpdateForm body)
            , expect =
                leExpectJson toMsg jsonDecUserMeetupInfo
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiUserMeetupsByMeetupidApplicationsjson : MeetupId -> (Result Error  ((List UserApplication))  -> msg) -> Cmd msg
getApiUserMeetupsByMeetupidApplicationsjson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "meetups"
                    , (capture_meetup_id |> String.fromInt)
                    , "applications.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecUserApplication))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiUserMeetupsApplicationByUserapplicationidAcceptjson : UserApplicationId -> (Result Error  (())  -> msg) -> Cmd msg
postApiUserMeetupsApplicationByUserapplicationidAcceptjson capture_user_application_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "meetups"
                    , "application"
                    , (capture_user_application_id
                       |> String.fromInt)
                    , "accept.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiUserMeetupsApplicationByUserapplicationidRejectjson : UserApplicationId -> (Result Error  (())  -> msg) -> Cmd msg
postApiUserMeetupsApplicationByUserapplicationidRejectjson capture_user_application_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "meetups"
                    , "application"
                    , (capture_user_application_id
                       |> String.fromInt)
                    , "reject.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                Http.expectString 
                     (\x -> case x of
                     Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})
                     Ok _ -> toMsg (Ok ()))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



getApiMeetupByMeetupidAcceptedmembersjson : MeetupId -> (Result Error  ((List PublicAccountInfo))  -> msg) -> Cmd msg
getApiMeetupByMeetupidAcceptedmembersjson capture_meetup_id toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "GET"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "meetup"
                    , (capture_meetup_id |> String.fromInt)
                    , "accepted-members.json"
                    ]
                    params
            , body =
                Http.emptyBody
            , expect =
                leExpectJson toMsg (Json.Decode.list (jsonDecPublicAccountInfo))
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiUserEventsCreatejson : EventUpdateForm -> (Result Error  (Event)  -> msg) -> Cmd msg
postApiUserEventsCreatejson body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "events"
                    , "create.json"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncEventUpdateForm body)
            , expect =
                leExpectJson toMsg jsonDecEvent
            , timeout =
                Nothing
            , tracker =
                Nothing
            }



postApiUserEventsByEventidUpdatejson : EventId -> EventUpdateForm -> (Result Error  (Event)  -> msg) -> Cmd msg
postApiUserEventsByEventidUpdatejson capture_event_id body toMsg =
    let
        params =
            List.filterMap identity
            (List.concat
                [])
    in
        Http.request
            { method =
                "POST"
            , headers =
                []
            , url =
                Url.Builder.crossOrigin ""
                    [ "api"
                    , "user"
                    , "events"
                    , (capture_event_id |> String.fromInt)
                    , "update.json"
                    ]
                    params
            , body =
                Http.jsonBody (jsonEncEventUpdateForm body)
            , expect =
                leExpectJson toMsg jsonDecEvent
            , timeout =
                Nothing
            , tracker =
                Nothing
            }

