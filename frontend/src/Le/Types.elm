module Le.Types exposing
    ( ViewParams
    , isMobile
    )

import Browser.Dom
import Time


type alias ViewParams =
    { isMobile : Bool
    , now : Maybe Time.Posix
    }


isMobile : Maybe Browser.Dom.Viewport -> Bool
isMobile x =
    case x of
        Nothing ->
            False

        Just vp ->
            if vp.viewport.width > 576 then
                False

            else
                True
