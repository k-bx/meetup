module Le.Pages.Catalog exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Http
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Lib exposing (..)
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Url.Builder as UrlBuilder


type Msg
    = CatalogLoaded (Result Api.Error (List Api.Meetup))
    | ToastMsg Le.Block.Toast.Msg
    | NeedLogin


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , meetups : List Api.Meetup
    }


init : ( Model, Cmd Msg )
init =
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , meetups = []
      }
    , Api.getApiMeetupsjson CatalogLoaded
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NeedLogin ->
            -- handled by parent update
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        CatalogLoaded (Err e) ->
            handleHttpError ToastMsg e model

        CatalogLoaded (Ok meetups) ->
            ( { model | meetups = meetups }
            , Cmd.none
            )


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        renderItem item =
            { title = a [ href <| Le.Routes.meetup item.id ] [ text item.title ]
            , content = p [] <| Markdown.toHtml Nothing item.description
            }
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text "Latest additions" ]) model <|
        [ catalogCardGrid (List.map renderItem model.meetups)
        ]
