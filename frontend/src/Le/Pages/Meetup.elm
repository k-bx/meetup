module Le.Pages.Meetup exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Lib exposing (..)
import Le.Ports
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Maybe.Extra
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | UpdateModel Model
    | NeedLogin
    | MeetupLoaded (Result Api.Error Api.Meetup)
    | EventsLoaded (Result Api.Error (List Api.Event))
    | TimeLoaded Time.Posix
    | AccountLoaded (Result Api.Error Api.AccountInfo)
    | Rsvp Api.Event
    | RsvpDone Api.Event (Result Api.Error Api.RsvpInfo)
    | RsvpListLoaded Api.Event (Result Api.Error (List Api.RsvpInfo))
    | UnRsvp Api.Event
    | UnRsvpDone Api.Event (Result Api.Error ())
    | GotMeetupAuthInfo (Result Api.Error Api.MeetupAuthInfo)
    | DeleteAplicationPressed
    | DeleteApplicationDone (Result Api.Error ())
    | GotAcceptedMembers (Result Api.Error (List Api.PublicAccountInfo))


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , meetupId : Int
    , now : Maybe Time.Posix
    , meetup : Maybe Api.Meetup
    , meetupAuthInfo : Maybe Api.MeetupAuthInfo
    , events : List Api.Event
    , account : Maybe Api.AccountInfo
    , eventRsvps : Dict Api.EventId (List Api.RsvpInfo)
    , acceptedMembers : List Api.PublicAccountInfo
    , showAllAcceptedMembers : Bool
    }


init : Int -> ( Model, Cmd Msg )
init meetupId =
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , meetupId = meetupId
      , now = Nothing
      , meetup = Nothing
      , meetupAuthInfo = Nothing
      , events = []
      , account = Nothing
      , eventRsvps = Dict.fromList []
      , acceptedMembers = []
      , showAllAcceptedMembers = False
      }
    , Cmd.batch
        [ Api.getApiMeetupByMeetupidInfojson meetupId MeetupLoaded
        , Api.getApiMeetupByMeetupidAuthinfojson meetupId GotMeetupAuthInfo
        , Api.getApiMeetupByMeetupidEventsjson meetupId EventsLoaded
        , nowCmd
        , Api.getApiAccountinfojson AccountLoaded
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NeedLogin ->
            ( model
            , Le.Ports.needLoginRedirect ()
            )

        NoOp ->
            ( model, Cmd.none )

        UpdateModel m2 ->
            ( m2, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        MeetupLoaded (Err e) ->
            handleHttpError ToastMsg e model

        MeetupLoaded (Ok meetup) ->
            ( { model | meetup = Just meetup }
            , Api.getApiMeetupByMeetupidAcceptedmembersjson model.meetupId GotAcceptedMembers
            )

        EventsLoaded (Err e) ->
            handleHttpError ToastMsg e model

        EventsLoaded (Ok events) ->
            ( { model | events = events }
            , Cmd.batch (List.map (\ev -> Api.getApiEventByEventidRsvpListjson ev.id (RsvpListLoaded ev)) events)
            )

        TimeLoaded now ->
            ( { model | now = Just now }, Cmd.none )

        AccountLoaded (Err e) ->
            handleHttpErrorNoRedirect ToastMsg e model

        AccountLoaded (Ok acc) ->
            ( { model | account = Just acc }, Cmd.none )

        Rsvp event ->
            ( model, Api.postApiEventByEventidRsvpCreatejson event.id (RsvpDone event) )

        RsvpDone event (Err e) ->
            handleHttpError ToastMsg e model

        RsvpDone event (Ok rsvp) ->
            ( { model | eventRsvps = dictSingletonOrUpdate event.id rsvp model.eventRsvps }
            , Le.Block.Toast.addInfo ToastMsg "You've RSVPd successfully"
            )

        UnRsvp event ->
            ( model, Api.deleteApiEventByEventidRsvpDeletejson event.id (UnRsvpDone event) )

        RsvpListLoaded event (Err e) ->
            handleHttpError ToastMsg e model

        RsvpListLoaded event (Ok rsvps) ->
            ( { model | eventRsvps = Dict.insert event.id rsvps model.eventRsvps }
            , Cmd.none
            )

        UnRsvpDone event (Err e) ->
            handleHttpError ToastMsg e model

        UnRsvpDone event (Ok ()) ->
            let
                mAccId =
                    case model.account of
                        Nothing ->
                            Nothing

                        Just acc ->
                            Just acc.id

                upd mOld =
                    case mOld of
                        Nothing ->
                            Nothing

                        Just old ->
                            Just (List.filter (\x -> not (Just x.user_id == mAccId)) old)

                newD =
                    Dict.update event.id upd model.eventRsvps
            in
            ( { model | eventRsvps = newD }
            , Le.Block.Toast.addInfo ToastMsg "You've un-RSVPd successfully"
            )

        GotMeetupAuthInfo (Err e) ->
            handleHttpErrorNoRedirect ToastMsg e model

        GotMeetupAuthInfo (Ok meetupAuthInfo) ->
            ( { model | meetupAuthInfo = Just meetupAuthInfo }
            , Cmd.none
            )

        DeleteAplicationPressed ->
            ( model
            , Api.postApiMeetupByMeetupidApplicationformDeletejson model.meetupId DeleteApplicationDone
            )

        DeleteApplicationDone (Err e) ->
            handleHttpError ToastMsg e model

        DeleteApplicationDone (Ok ()) ->
            ( model
            , Api.getApiMeetupByMeetupidAuthinfojson model.meetupId GotMeetupAuthInfo
            )

        GotAcceptedMembers (Err e) ->
            handleHttpError ToastMsg e model

        GotAcceptedMembers (Ok members) ->
            ( { model | acceptedMembers = members }
            , Cmd.none
            )


nowCmd : Cmd Msg
nowCmd =
    Task.perform TimeLoaded Time.now


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        titleSuffix =
            model.meetup |> Maybe.map (\meetup -> meetup.title) |> fromMaybe ""

        rsvpButtonBlockWrapped meetup event =
            case model.meetupAuthInfo of
                Nothing ->
                    rsvpButtonBlock event

                Just meetupAuthInfo ->
                    case meetup.member_approval_required of
                        False ->
                            rsvpButtonBlock event

                        True ->
                            case meetupAuthInfo.approved_member_id of
                                Just _ ->
                                    rsvpButtonBlock event

                                Nothing ->
                                    case meetupAuthInfo.user_application_id of
                                        Nothing ->
                                            needApprovalForm

                                        Just userApplicationId ->
                                            case meetupAuthInfo.membership_approved of
                                                Nothing ->
                                                    beingReviewedForm

                                                Just True ->
                                                    rsvpButtonBlock event

                                                Just False ->
                                                    rejected

        rejected =
            text "We're sorry, but you've been rejected from applying to this event, so you cannot RSVP. Please contact event organizers for more info."

        needApprovalForm =
            div [ class "row" ]
                [ div [ class "col-md-6 bd-callout bd-callout-warning bg-light" ]
                    [ div [ class "mb-2" ]
                        [ text "This event needs organizer's approval to enroll." ]
                    , a [ href <| Le.Routes.meetupApplicationForm model.meetupId ]
                        [ button [ class "btn btn-primary btn-sm" ]
                            [ text "Fill the Application Form" ]
                        ]
                    ]
                ]

        beingReviewedForm =
            div [ class "row" ]
                [ div [ class "col-md-6 bd-callout bd-callout-warning bg-light" ]
                    [ div [ class "mb-2" ]
                        [ text "Your application is under a review." ]
                    , a [ href <| Le.Routes.meetupApplicationForm model.meetupId ]
                        [ button [ class "btn btn-primary btn-sm mr-2" ]
                            [ text "Edit My Application" ]
                        ]
                    , button
                        [ class "btn btn-danger btn-sm"
                        , onClick DeleteAplicationPressed
                        ]
                        [ text "Delete My Application" ]
                    ]
                ]

        rsvpButtonBlock event =
            case model.account of
                Nothing ->
                    Button.button
                        [ Button.primary
                        , Button.attrs [ onClick NeedLogin ]
                        ]
                        [ text "Log in to RSVP" ]

                Just acc ->
                    let
                        isRsvpAlready =
                            List.member acc.id (List.map .user_id (fromMaybe [] (Dict.get event.id model.eventRsvps)))
                    in
                    if isRsvpAlready then
                        Button.button
                            [ Button.primary
                            , Button.attrs [ onClick (UnRsvp event) ]
                            ]
                            [ text "un-RSVP" ]

                    else
                        Button.button
                            [ Button.primary
                            , Button.attrs [ onClick (Rsvp event) ]
                            ]
                            [ text "RSVP" ]

        futureEvents meetup =
            let
                events =
                    Just (\now -> List.filter (\ev -> ev.date >= Time.posixToMillis now) model.events)
                        |> Maybe.Extra.andMap model.now
                        |> fromMaybe []
                        |> List.sortBy .date

                renderEvent ev =
                    Card.config [ Card.attrs [ class "mb-4 shadow-lg" ] ]
                        |> Card.block []
                            [ Block.titleH4 [] [ text (renderDateStr ev.date) ]
                            , Block.text [] [ locationBlock ev ]
                            , Block.text [] [ rsvpButtonBlockWrapped meetup ev ]
                            , Block.text [] [ rsvpInfoBlock model.eventRsvps ev ]
                            ]
                        |> Card.view
            in
            [ h2 [] [ text "Future events" ]
            ]
                ++ (case events of
                        [] ->
                            [ text "No future events" ]

                        _ ->
                            List.map renderEvent events
                   )
                ++ [ div [ class "mb-4" ] [] ]

        pastEvents meetup =
            let
                events =
                    Just (\now -> List.filter (\ev -> ev.date < Time.posixToMillis now) model.events)
                        |> Maybe.Extra.andMap model.now
                        |> fromMaybe []
                        |> List.sortBy .date

                renderEvent ev =
                    Card.config [ Card.attrs [ class "mb-4 shadow-lg" ] ]
                        |> Card.block []
                            [ Block.titleH4 [] [ text (renderDateStr ev.date) ]
                            , Block.text [] [ locationBlock ev ]
                            , Block.text [] [ rsvpInfoBlock model.eventRsvps ev ]
                            ]
                        |> Card.view
            in
            [ h2 [] [ text "Past events" ]
            ]
                ++ (case events of
                        [] ->
                            [ text "No past events" ]

                        _ ->
                            List.map renderEvent events
                   )
                ++ [ div [ class "mb-4" ] [] ]

        acceptedMembersBlock meetup =
            case meetup.member_approval_required of
                False ->
                    div [] []

                True ->
                    let
                        membersNum =
                            String.fromInt (List.length model.acceptedMembers)
                    in
                    div []
                        [ h3 [] [ text <| "Accepted members (" ++ membersNum ++ "):" ]
                        , ul [] <| renderAcceptedMembers model.acceptedMembers
                        ]

        renderAcceptedMembers acceptedMembers =
            if model.showAllAcceptedMembers then
                List.map renderAcceptedMember model.acceptedMembers

            else if List.length acceptedMembers <= 3 then
                List.map renderAcceptedMember model.acceptedMembers

            else
                List.map renderAcceptedMember (List.take 3 model.acceptedMembers)
                    ++ [ li []
                            [ span
                                [ class "link-like"
                                , onClick <| UpdateModel { model | showAllAcceptedMembers = True }
                                ]
                                [ text "show all" ]
                            ]
                       ]

        renderAcceptedMember : Api.PublicAccountInfo -> Html Msg
        renderAcceptedMember member =
            li []
                [ text <| fromMaybe "(unknown)" member.full_name
                ]

        renderMeetup meetup =
            div [] <|
                Markdown.toHtml Nothing meetup.description
                    ++ [ acceptedMembersBlock meetup ]
                    ++ futureEvents meetup
                    ++ pastEvents meetup

        inner =
            case model.meetup of
                Just meetup ->
                    renderMeetup meetup

                _ ->
                    loadingSpinner
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text titleSuffix ]) model <|
        [ inner ]
