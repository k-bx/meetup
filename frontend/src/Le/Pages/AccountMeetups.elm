module Le.Pages.AccountMeetups exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Navbar as Navbar
import Browser.Navigation as Navigation
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Lib exposing (..)
import Le.Pages.AccountCommon
import Le.Ports as Ports
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | LogOutPressed
    | NavbarMsg Navbar.State
    | GotMeetups (Result Api.Error (List Api.Meetup))


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , accInfo : Maybe Api.AccountInfo
    , navbar : Navbar.State
    , meetups : List Api.Meetup
    }


init : ( Model, Cmd Msg )
init =
    let
        ( navbar, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , accInfo = Nothing
      , navbar = navbar
      , meetups = []
      }
    , Cmd.batch
        [ Api.getApiAccountinfojson GotAccountInfo
        , navbarCmd
        , Api.getApiUserMeetupsjson GotMeetups
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        NavbarMsg state ->
            ( { model | navbar = state }, Cmd.none )

        LogOutPressed ->
            ( model, Navigation.load Le.Routes.logOut )

        GotAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( handleHttpSuccess { model | accInfo = Just accInfo }
                , Cmd.none
                )

            else
                ( handleHttpSuccess model
                , Ports.needLoginRedirect ()
                )

        GotMeetups (Err e) ->
            handleHttpError ToastMsg e model

        GotMeetups (Ok meetups) ->
            ( { model | meetups = meetups }, Cmd.none )


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        renderItem item =
            { title = a [ href <| Le.Routes.accountMeetupInfo item.id ] [ text item.title ]
            , content = div [] []
            }
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text "Your Meetups" ]) model <|
        [ Le.Pages.AccountCommon.nav NavbarMsg LogOutPressed model.navbar
        , div [ class "mb-4 ml-4" ]
            [ a [ href <| Le.Routes.accountCreateMeetup ] [ button [ class "btn btn-secondary btn-sm" ] [ text "Create Meetup" ] ]
            ]
        , catalogCardGrid (List.map renderItem model.meetups)
        ]
