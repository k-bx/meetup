module Le.Pages.AccountMeetupInfo exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Navbar as Navbar
import Browser.Navigation as Navigation
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Components.Markdown
import Le.Lib exposing (..)
import Le.Pages.AccountCommon
import Le.Ports as Ports
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Maybe.Extra
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | LogOutPressed
    | NavbarMsg Navbar.State
    | GotMeetup (Result Api.Error Api.UserMeetupInfo)
    | GotMeetupApplications (Result Api.Error (List Api.UserApplication))
    | GotPublicAccountInfo (Result Api.Error Api.PublicAccountInfo)
    | AcceptClicked Api.UserApplication
    | RejectClicked Api.UserApplication
    | AcceptOrRejectDone Api.UserApplication (Result Api.Error ())
    | GotApplicationForm (Result Api.Error Api.ApplicationForm)


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , meetupId : Api.MeetupId
    , accInfo : Maybe Api.AccountInfo
    , navbar : Navbar.State
    , meetup : Maybe Api.UserMeetupInfo
    , applications : List Api.UserApplication
    , users : Dict Api.UserId Api.PublicAccountInfo
    , applicationForm : Maybe Api.ApplicationForm
    }


init : Api.MeetupId -> ( Model, Cmd Msg )
init meetupId =
    let
        ( navbar, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , meetupId = meetupId
      , accInfo = Nothing
      , navbar = navbar
      , meetup = Nothing
      , applications = []
      , users = Dict.empty
      , applicationForm = Nothing
      }
    , Cmd.batch
        [ Api.getApiAccountinfojson GotAccountInfo
        , navbarCmd
        , Api.getApiUserMeetupsByMeetupidInfojson meetupId GotMeetup
        , Api.getApiUserMeetupsByMeetupidApplicationsjson meetupId GotMeetupApplications
        , Api.getApiMeetupByMeetupidApplicationformjson meetupId GotApplicationForm
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        NavbarMsg state ->
            ( { model | navbar = state }, Cmd.none )

        LogOutPressed ->
            ( model, Navigation.load Le.Routes.logOut )

        GotAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( handleHttpSuccess { model | accInfo = Just accInfo }
                , Cmd.none
                )

            else
                ( handleHttpSuccess model
                , Ports.needLoginRedirect ()
                )

        GotMeetup (Err e) ->
            handleHttpError ToastMsg e model

        GotMeetup (Ok meetup) ->
            ( { model | meetup = Just meetup }
            , Cmd.none
            )

        GotMeetupApplications (Err e) ->
            handleHttpError ToastMsg e model

        GotMeetupApplications (Ok applications) ->
            let
                userIds =
                    applications
                        |> List.map .user_id

                userCmds =
                    userIds
                        |> List.map (\x -> Api.getApiUserByUseridPublicaccountinfojson x GotPublicAccountInfo)
            in
            ( { model | applications = applications }
            , Cmd.batch userCmds
            )

        GotPublicAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotPublicAccountInfo (Ok user) ->
            ( { model | users = Dict.insert user.id user model.users }
            , Cmd.none
            )

        AcceptClicked application ->
            ( model
            , Api.postApiUserMeetupsApplicationByUserapplicationidAcceptjson application.id (AcceptOrRejectDone application)
            )

        RejectClicked application ->
            ( model
            , Api.postApiUserMeetupsApplicationByUserapplicationidRejectjson application.id (AcceptOrRejectDone application)
            )

        AcceptOrRejectDone application (Err e) ->
            handleHttpError ToastMsg e model

        AcceptOrRejectDone application (Ok user) ->
            ( model
            , Api.getApiUserMeetupsByMeetupidApplicationsjson model.meetupId GotMeetupApplications
            )

        GotApplicationForm (Err e) ->
            case e.httpError of
                Http.BadStatus 404 ->
                    ( { model | applicationForm = Nothing }, Cmd.none )

                _ ->
                    handleHttpError ToastMsg e model

        GotApplicationForm (Ok applicationForm) ->
            ( { model | applicationForm = Just applicationForm }, Cmd.none )


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        renderItem item =
            { title = a [ href <| Le.Routes.accountMeetupInfo item.id ] [ text item.title ]
            , content = div [] []
            }

        genField =
            formGenField model.formErrors

        textField =
            formTextField model.formErrors

        renderApplicationField : Api.Meetup -> Api.ApplicationForm -> Api.UserApplicationField -> Html Msg
        renderApplicationField meetup applicationForm field =
            let
                mformField =
                    applicationForm.fields
                        |> List.filter (\x -> x.id == field.field_id)
                        |> List.head

                questionDiv =
                    mformField
                        |> Maybe.map (\formField -> div [] (Markdown.toHtml Nothing formField.description))
                        |> fromMaybe (div [] [])
            in
            div []
                [ div [ class "d-flex" ]
                    [ strong [ class "d-block mr-2" ] [ text "Q: " ]
                    , questionDiv
                    ]
                , div [] (Markdown.toHtml Nothing field.content)
                ]

        renderApplication : Api.Meetup -> Api.ApplicationForm -> Api.UserApplication -> Html Msg
        renderApplication meetup applicationForm application =
            let
                userFullName =
                    Dict.get application.user_id model.users
                        |> Maybe.map .full_name
                        |> Maybe.Extra.join
                        |> fromMaybe "(unknown)"

                buttonsOrStatusBlock =
                    case application.membership_approved of
                        Nothing ->
                            buttonsBlock

                        Just True ->
                            span [ class "text-success" ] [ text "Approved" ]

                        Just False ->
                            span [ class "text-danger" ] [ text "Declined" ]

                buttonsBlock =
                    div []
                        [ button
                            [ class "btn btn-sm btn-outline-success mr-2"
                            , onClick <| AcceptClicked application
                            ]
                            [ text "Accept" ]
                        , button
                            [ class "btn btn-sm btn-outline-danger"
                            , onClick <| RejectClicked application
                            ]
                            [ text "Reject" ]
                        ]
            in
            div [ class "card mb-4 bg-light border-0 rounded-0 shadow-sm" ]
                [ div [ class "card-body" ]
                    [ div [ class "mb-2" ] [ strong [] [ text userFullName ] ]
                    , div [] <| List.map (renderApplicationField meetup applicationForm) application.fields
                    , buttonsOrStatusBlock
                    ]
                ]

        membersWhoNeedApproval meetup applicationForm =
            div [ class "mb-4" ]
                [ h3 [] [ text "Applications awaiting approval" ]
                , div [] (List.map (renderApplication meetup applicationForm) model.applications)
                ]

        renderFormField field =
            div [ class "card mb-4 bg-light border-0 rounded-0 shadow-sm" ]
                [ div [ class "card-body" ] <|
                    Markdown.toHtml Nothing field.description
                ]

        renderMeetup meetup =
            div [ class "mb-4" ]
                [ h2 [ class "mb-2" ]
                    [ text meetup.title
                    , a [ href <| Le.Routes.accountMeetupEdit model.meetupId ]
                        [ button [ class "btn btn-secondary ml-2" ] [ text "edit" ] ]
                    , a [ href <| Le.Routes.accountMeetupEvents model.meetupId ]
                        [ button [ class "btn btn-secondary ml-2" ]
                            [ text "events" ]
                        ]
                    ]
                , div [] (Markdown.toHtml Nothing meetup.description)
                , div [ class "mb-4" ]
                    [ strong [] [ text "Member approval required: " ]
                    , text
                        (if meetup.member_approval_required then
                            "yes"

                         else
                            "no"
                        )
                    ]
                , case model.applicationForm of
                    Nothing ->
                        case meetup.member_approval_required of
                            True ->
                                loadingSpinner

                            False ->
                                div [] []

                    Just applicationForm ->
                        membersWhoNeedApproval meetup applicationForm
                ]
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text "Meetup Info" ]) model <|
        [ Le.Pages.AccountCommon.nav NavbarMsg LogOutPressed model.navbar
        , case model.meetup of
            Nothing ->
                loadingSpinner

            Just meetup ->
                renderMeetup meetup
        ]
