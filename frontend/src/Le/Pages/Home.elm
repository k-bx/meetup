module Le.Pages.Home exposing (view)

import Html exposing (..)
import Markdown


view : List (Html msg)
view =
    [ h1 [] [ text "Welcome" ]
    , p []
        (Markdown.toHtml Nothing
            """

Welcome to Meedup.Events. Open source Elm+Haskell app where you can
organize a meetup with ability for others to RSVP.

Sources can be found at our [GitLab](https://gitlab.com/k-bx/meetup)
                """
        )
    ]
