module Le.Pages.FeedbackForm exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as J
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Components.Markdown
import Le.Lib exposing (handleHttpError)
import Le.Ports as Ports
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | GotFeedbackForm (Result Api.Error Api.FeedbackForm)
    | GotFeedback (Result Api.Error Api.Feedback)
    | FormChanged Model
    | SavePressed
    | SaveDone (Result Api.Error Api.Feedback)


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , feedbackFormId : Api.FeedbackFormId
    , accInfo : Maybe Api.AccountInfo
    , feedbackForm : Maybe Api.FeedbackForm
    , content : String
    , complete : Bool
    }


init : Api.FeedbackFormId -> ( Model, Cmd Msg )
init feedbackFormId =
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , feedbackFormId = feedbackFormId
      , accInfo = Nothing
      , feedbackForm = Nothing
      , content = ""
      , complete = True
      }
    , Cmd.batch
        [ Api.getApiAccountinfojson GotAccountInfo
        , Api.getApiFeedbackformsByFeedbackformidInfojson feedbackFormId GotFeedbackForm
        , Api.getApiFeedbackformsByFeedbackformidFeedbackjson feedbackFormId GotFeedback
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        GotAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( { model | accInfo = Just accInfo }
                , Cmd.none
                )

            else
                ( model
                , Ports.needLoginRedirect ()
                )

        GotFeedbackForm (Err e) ->
            handleHttpError ToastMsg e model

        GotFeedbackForm (Ok feedbackForm) ->
            ( { model | feedbackForm = Just feedbackForm }, Cmd.none )

        GotFeedback (Err e) ->
            handleHttpError ToastMsg e model

        GotFeedback (Ok feedback) ->
            ( { model
                | content = feedback.content
                , complete = feedback.complete
              }
            , Cmd.none
            )

        FormChanged m2 ->
            ( m2, Cmd.none )

        SavePressed ->
            ( model
            , Api.postApiFeedbackformsByFeedbackformidSetfeedbackjson model.feedbackFormId
                { content = model.content
                , complete = model.complete
                }
                SaveDone
            )

        SaveDone (Err e) ->
            handleHttpError ToastMsg e model

        SaveDone (Ok _) ->
            ( model
            , Le.Block.Toast.addInfo ToastMsg "Your feedback saved successfully"
            )


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        title =
            case model.feedbackForm of
                Nothing ->
                    "Feedback Form"

                Just f ->
                    "Feedback Form: " ++ f.title

        genField =
            formGenField model.formErrors

        textField =
            formTextField model.formErrors

        renderForm =
            case model.feedbackForm of
                Nothing ->
                    [ text "Loading..." ]

                Just f ->
                    renderFormJust f

        renderFormJust form =
            Markdown.toHtml Nothing form.description
                ++ [ genField "Feedback" "content" <|
                        Le.Components.Markdown.editor
                            model.content
                            (\v -> FormChanged { model | content = v })
                   , genField "Response is complete (considered draft otherwise)" "complete" <|
                        div []
                            [ input
                                [ type_ "checkbox"
                                , checked model.complete
                                , onClick (FormChanged { model | complete = not model.complete })
                                ]
                                []
                            ]
                   , button
                        [ class "btn btn-primary"
                        , onClick SavePressed
                        ]
                        [ text "Save"
                        ]
                   ]
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text title ]) model <|
        renderForm
