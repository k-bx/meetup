module Le.Pages.AccountCommon exposing (..)

import Bootstrap.Navbar as Navbar
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Le.Routes


nav navbarMsg logOutPressed navbar =
    Navbar.config navbarMsg
        |> Navbar.items
            [ Navbar.itemLink [ href Le.Routes.account ] [ text "General" ]
            , Navbar.itemLink [ href Le.Routes.accountMeetups ] [ text "Your Meetups" ]
            , Navbar.itemLink
                [ class "link-like"
                , onClick logOutPressed
                ]
                [ text "Log Out" ]
            ]
        |> Navbar.view navbar
