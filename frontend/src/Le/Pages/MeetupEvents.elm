module Le.Pages.MeetupEvents exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Navbar as Navbar
import Browser.Navigation as Navigation
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Components.Markdown
import Le.Lib exposing (..)
import Le.Pages.AccountCommon
import Le.Ports as Ports
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Maybe.Extra
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | LogOutPressed
    | NavbarMsg Navbar.State
    | GotMeetup (Result Api.Error Api.UserMeetupInfo)
    | GotEvents (Result Api.Error (List Api.Event))
    | RsvpListLoaded Api.Event (Result Api.Error (List Api.RsvpInfo))


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , meetupId : Api.MeetupId
    , accInfo : Maybe Api.AccountInfo
    , navbar : Navbar.State
    , meetup : Maybe Api.UserMeetupInfo
    , events : List Api.Event
    , eventRsvps : Dict Api.EventId (List Api.RsvpInfo)
    }


init : Api.MeetupId -> ( Model, Cmd Msg )
init meetupId =
    let
        ( navbar, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , meetupId = meetupId
      , accInfo = Nothing
      , navbar = navbar
      , meetup = Nothing
      , events = []
      , eventRsvps = Dict.empty
      }
    , Cmd.batch
        [ Api.getApiAccountinfojson GotAccountInfo
        , navbarCmd
        , Api.getApiUserMeetupsByMeetupidInfojson meetupId GotMeetup
        , Api.getApiMeetupByMeetupidEventsjson meetupId GotEvents
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        NavbarMsg state ->
            ( { model | navbar = state }, Cmd.none )

        LogOutPressed ->
            ( model, Navigation.load Le.Routes.logOut )

        GotAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( handleHttpSuccess { model | accInfo = Just accInfo }
                , Cmd.none
                )

            else
                ( handleHttpSuccess model
                , Ports.needLoginRedirect ()
                )

        GotMeetup (Err e) ->
            handleHttpError ToastMsg e model

        GotMeetup (Ok meetup) ->
            ( { model | meetup = Just meetup }
            , Cmd.none
            )

        GotEvents (Err e) ->
            handleHttpError ToastMsg e model

        GotEvents (Ok events) ->
            let
                events2 =
                    List.reverse (List.sortBy .date events)
            in
            ( { model | events = events2 }
            , Cmd.batch (List.map (\ev -> Api.getApiEventByEventidRsvpListjson ev.id (RsvpListLoaded ev)) events)
            )

        RsvpListLoaded event (Err e) ->
            handleHttpError ToastMsg e model

        RsvpListLoaded event (Ok rsvps) ->
            ( { model | eventRsvps = Dict.insert event.id rsvps model.eventRsvps }
            , Cmd.none
            )


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        renderItem item =
            { title = a [ href <| Le.Routes.accountMeetupInfo item.id ] [ text item.title ]
            , content = div [] []
            }

        genField =
            formGenField model.formErrors

        textField =
            formTextField model.formErrors

        renderApplicationField : Api.Meetup -> Api.ApplicationForm -> Api.UserApplicationField -> Html Msg
        renderApplicationField meetup applicationForm field =
            let
                mformField =
                    applicationForm.fields
                        |> List.filter (\x -> x.id == field.field_id)
                        |> List.head

                questionDiv =
                    mformField
                        |> Maybe.map (\formField -> div [] (Markdown.toHtml Nothing formField.description))
                        |> fromMaybe (div [] [])
            in
            div []
                [ div [ class "d-flex" ]
                    [ strong [ class "d-block mr-2" ] [ text "Q: " ]
                    , questionDiv
                    ]
                , div [] (Markdown.toHtml Nothing field.content)
                ]

        renderFormField field =
            div [ class "card mb-4 bg-light border-0 rounded-0 shadow-sm" ]
                [ div [ class "card-body" ] <|
                    Markdown.toHtml Nothing field.description
                ]

        renderEvent event =
            div [ class "card mb-4 shadow-lg" ]
                [ div [ class "card-body" ]
                    [ div [ class "card-title" ]
                        [ h4 []
                            [ text (renderDateStr event.date)
                            , a
                                [ class "btn btn-secondary btn-sm ml-2"
                                , href <| Le.Routes.accountEventEdit event.meetup_id event.id
                                ]
                                [ text "edit" ]
                            ]
                        ]
                    , div [ class "card-text mb-2" ]
                        [ locationBlock event ]
                    , div [ class "card-text" ]
                        [ rsvpInfoBlock model.eventRsvps event ]
                    ]
                ]

        eventsBlock =
            div [] <| List.map renderEvent model.events

        renderMeetup meetup =
            div [ class "mb-4" ]
                [ h2 [ class "mb-2" ]
                    [ a [ href <| Le.Routes.accountMeetupInfo model.meetupId ] [ text meetup.title ] ]
                , a [ href <| Le.Routes.accountEventCreate meetup.id ]
                    [ button [ class "btn btn-secondary btn-sm" ]
                        [ text "create" ]
                    ]
                , div [ class "mt-4" ] [ eventsBlock ]
                ]
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text "Events List" ]) model <|
        [ Le.Pages.AccountCommon.nav NavbarMsg LogOutPressed model.navbar
        , case model.meetup of
            Nothing ->
                loadingSpinner

            Just meetup ->
                renderMeetup meetup
        ]
