module Le.Util exposing (..)

import Calendar
import Clock
import DateTime
import Dict exposing (Dict)
import Http
import Le.Api as Api
import Time exposing (Month(..), Weekday(..))


showHttpError : Http.Error -> String
showHttpError e =
    case e of
        Http.BadUrl s ->
            "Bad url: " ++ s

        Http.Timeout ->
            "Timeout"

        Http.NetworkError ->
            "Network error"

        Http.BadStatus _ ->
            "Bad status"

        Http.BadBody _ ->
            "Bad body"


showCondition : Api.Condition -> String
showCondition x =
    case x of
        Api.New ->
            "New"

        Api.LikeNew ->
            "Like new"

        Api.VeryGood ->
            "Very Good"

        Api.Good ->
            "Good"

        Api.Acceptable ->
            "Acceptable"

        Api.BetterThanNothing ->
            "Better Than Nothing"


splitByN : Int -> List a -> List (List a)
splitByN i list =
    case List.take i list of
        [] ->
            []

        listHead ->
            listHead :: splitByN i (List.drop i list)


keepOks : List (Result a b) -> List b
keepOks xss =
    case xss of
        [] ->
            []

        x :: xs ->
            case x of
                Err _ ->
                    keepOks xs

                Ok v ->
                    v :: keepOks xs


mapOk : (b -> c) -> Result a b -> Result a c
mapOk f x =
    case x of
        Err e ->
            Err e

        Ok v ->
            Ok (f v)


either : (a -> c) -> (b -> c) -> Result a b -> c
either fa fb res =
    case res of
        Err a ->
            fa a

        Ok b ->
            fb b


mapBoth : (a -> c) -> (b -> d) -> Result a b -> Result c d
mapBoth fa fb res =
    case res of
        Err a ->
            Err (fa a)

        Ok b ->
            Ok (fb b)


fromMaybe : a -> Maybe a -> a
fromMaybe =
    Maybe.withDefault


maybe : a -> (b -> a) -> Maybe b -> a
maybe a f mb =
    case mb of
        Nothing ->
            a

        Just b ->
            f b


renderDateStr dt =
    let
        p =
            Time.millisToPosix dt

        z =
            Time.utc
    in
    displayWeekday (Time.toWeekday z p)
        ++ ", "
        ++ String.fromInt (Time.toDay z p)
        ++ " "
        ++ displayMonth (Time.toMonth z p)
        ++ ", "
        ++ String.fromInt (Time.toYear z p)
        ++ ", "
        ++ intToStringTwoChars (Time.toHour z p)
        ++ ":"
        ++ intToStringTwoChars (Time.toMinute z p)


displayWeekday : Time.Weekday -> String
displayWeekday d =
    case d of
        Mon ->
            "Monday"

        Tue ->
            "Tuesday"

        Wed ->
            "Wednesday"

        Thu ->
            "Thursday"

        Fri ->
            "Friday"

        Sat ->
            "Saturday"

        Sun ->
            "Sunday"


displayMonth : Time.Month -> String
displayMonth m =
    case m of
        Jan ->
            "January"

        Feb ->
            "February"

        Mar ->
            "March"

        Apr ->
            "April"

        May ->
            "May"

        Jun ->
            "June"

        Jul ->
            "July"

        Aug ->
            "August"

        Sep ->
            "September"

        Oct ->
            "October"

        Nov ->
            "November"

        Dec ->
            "December"


monthToInt : Time.Month -> Int
monthToInt m =
    case m of
        Jan ->
            1

        Feb ->
            2

        Mar ->
            3

        Apr ->
            4

        May ->
            5

        Jun ->
            6

        Jul ->
            7

        Aug ->
            8

        Sep ->
            9

        Oct ->
            10

        Nov ->
            11

        Dec ->
            12


intToMonth : Int -> Maybe Time.Month
intToMonth i =
    case i of
        1 ->
            Just Jan

        2 ->
            Just Feb

        3 ->
            Just Mar

        4 ->
            Just Apr

        5 ->
            Just May

        6 ->
            Just Jun

        7 ->
            Just Jul

        8 ->
            Just Aug

        9 ->
            Just Sep

        10 ->
            Just Oct

        11 ->
            Just Nov

        12 ->
            Just Dec

        _ ->
            Nothing


intToStringTwoChars : Int -> String
intToStringTwoChars n =
    (if n < 10 then
        "0"

     else
        ""
    )
        ++ String.fromInt n


dictSingletonOrUpdate : comparable -> a -> Dict comparable (List a) -> Dict comparable (List a)
dictSingletonOrUpdate k v d =
    let
        upd mOld =
            case mOld of
                Nothing ->
                    Just [ v ]

                Just old ->
                    Just (old ++ [ v ])
    in
    Dict.update k upd d


{-| Render date suitable by value field of `input type="date"`
-}
renderDateValue t =
    String.fromInt (Time.toYear Time.utc t)
        ++ "-"
        ++ intToStringTwoChars (monthToInt (Time.toMonth Time.utc t))
        ++ "-"
        ++ intToStringTwoChars (Time.toDay Time.utc t)


{-| Parse date suitable by value field of `input type="date"`
-}
parseDateValue s date =
    case String.split "-" s of
        [ y, m, d ] ->
            case ( String.toInt y, String.toInt m, String.toInt d ) of
                ( Just yi, Just mi, Just di ) ->
                    case intToMonth mi of
                        Just mm ->
                            case Calendar.fromRawParts { day = di, month = mm, year = yi } of
                                Just dt ->
                                    Just (DateTime.toPosix (DateTime.fromDateAndTime dt (Clock.fromPosix date)))

                                Nothing ->
                                    Nothing

                        Nothing ->
                            Nothing

                _ ->
                    Nothing

        _ ->
            Nothing


{-| Render time suitable by value field of `input type="time"`
-}
renderTimeValue t =
    intToStringTwoChars (Time.toHour Time.utc t)
        ++ ":"
        ++ intToStringTwoChars (Time.toMinute Time.utc t)


{-| Parse time suitable by value field of `input type="time"`
-}
parseTimeValue s date =
    case String.split ":" s of
        [ h, m ] ->
            case ( String.toInt h, String.toInt m ) of
                ( Just hi, Just mi ) ->
                    case Clock.fromRawParts { hours = hi, minutes = mi, seconds = 0, milliseconds = 0 } of
                        Just t ->
                            Just (DateTime.toPosix (DateTime.fromDateAndTime (Calendar.fromPosix date) t))

                        Nothing ->
                            Nothing

                _ ->
                    Nothing

        _ ->
            Nothing
