module Le.Components.Markdown exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as J
import Json.Encode as Json


mdEditor attrs children =
    node "markdown-editor" attrs children


editor content onChange =
    mdEditor
        [ property "val"
            (Json.string content)
        , on "md-input" (J.map onChange (J.at [ "detail", "content" ] J.string))
        ]
        []
