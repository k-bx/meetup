module Le.Routes exposing (..)


home =
    "/"


meetups =
    "/meetups"


meetup i =
    "/meetup/" ++ String.fromInt i


account =
    "/account"


feedbackForm i =
    "/feedback-form/" ++ String.fromInt i


logOut =
    "/api/log-out"


meetupApplicationForm i =
    "/meetup/" ++ String.fromInt i ++ "/application-form"


accountMeetups =
    "/account/meetups"


accountMeetupInfo i =
    "/account/meetups/" ++ String.fromInt i ++ "/info"


accountMeetupEdit i =
    "/account/meetups/" ++ String.fromInt i ++ "/edit"


accountMeetupEvents i =
    "/account/meetups/" ++ String.fromInt i ++ "/events"


accountCreateMeetup =
    "/account/meetups/create"


accountEventCreate m =
    "/account/meetups/" ++ String.fromInt m ++ "/events/create"


accountEventEdit m i =
    "/account/meetups/" ++ String.fromInt m ++ "/events/" ++ String.fromInt i ++ "/edit"
