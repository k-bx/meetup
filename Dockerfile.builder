# Image that will be used to run `stack build` in it. Cannot use
# multi-stage builds because we want to mount your local ~/.stack to
# reuse Stack's cache, which cannot be done at `docker build`
FROM ubuntu:18.04

ENV \
    STACK_VERSION=2.1.3 \
    LTS_RESOLVER=lts-15.1

RUN set -ex; \
    touch /build.sh; \
    ln -s /build.sh /usr/local/bin/build
CMD ["/build.sh"]

RUN set -ex; \
    apt-get -qq update; \
    DEBIAN_FRONTEND=noninteractive apt-get -qqy install --no-install-recommends \
        `# Stack dependencies:` \
        g++ \
        gcc \
        libc6-dev \
        libffi-dev \
        libgmp-dev \
        make \
        xz-utils \
        zlib1g-dev \
        git \
        gnupg \
        wget \
        curl \
        ca-certificates \
        build-essential \
        netbase; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

# Install Stack to /usr/bin/stack:
#
RUN set -ex; \
    cd /usr/bin; \
    wget -q -O stack.tar.gz https://github.com/commercialhaskell/stack/releases/download/v${STACK_VERSION}/stack-${STACK_VERSION}-linux-x86_64.tar.gz; \
    tar xf stack.tar.gz; \
    rm stack.tar.gz; \
    ln -s /usr/bin/stack-${STACK_VERSION}-linux-x86_64/stack stack

# HACK:
#
# The entrypoint will create a builder user to match the developer's uid and
# gid on the host and then run stack under builder. However, above we install
# stack and npm packages as root. This hits us with these issues:
#
# 1. The /root/.stack-docker and /root/.npm directories contain files needed by the
#    builder user, but those files are owned by root.
# 2. Docker's AUFS image storage backend (default on Ubuntu) has a bug that
#    causes /root to be inaccessible to a non-root user even if it has the
#    right permissions: https://github.com/docker/docker/issues/783.
#
# The hack below combines into one layer the workaround to the above as
# described here:
# https://github.com/docker/docker/issues/783#issuecomment-56013588
#
RUN set -ex; \
    echo "hack"; \
    mv /root /root2; \
    mv /root2 /root; \
    chmod -R ago+rwx /root
# build deps; will be merged on top once they'll stabilise
RUN set -ex; \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg.list; \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -; \
    apt-get -qq update; \
    DEBIAN_FRONTEND=noninteractive apt-get -qqy install --no-install-recommends \
        libpq-dev; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN set -ex; \
    curl -L -o elm.gz https://github.com/elm/compiler/releases/download/0.19.1/binary-for-linux-64-bit.gz; \
    gunzip elm.gz; \
    chmod +x elm; \
    mv elm /usr/local/bin/; \
    elm --version

RUN set -ex; \
    curl -sL https://deb.nodesource.com/setup_10.x | bash -; \
    DEBIAN_FRONTEND=noninteractive apt-get -qqy install --no-install-recommends \
      nodejs; \
    npm install -g uglify-js sass

ADD pkgs/build.sh /build.sh
