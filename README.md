# Meetup.Events

https://meetup.events

Meetup.Events: open-source project where you can register your meetup, giving ability to RSVP to your guests. Serves as a showcase real-world app for Haskell + Elm.

Develop with:

```
./run-dev-webapp.hs
```
